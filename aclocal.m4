dnl aclocal.m4 generated automatically by aclocal 1.4-p5

dnl Copyright (C) 1994, 1995-8, 1999, 2001 Free Software Foundation, Inc.
dnl This file is free software; the Free Software Foundation
dnl gives unlimited permission to copy and/or distribute it,
dnl with or without modifications, as long as this notice is preserved.

dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY, to the extent permitted by law; without
dnl even the implied warranty of MERCHANTABILITY or FITNESS FOR A
dnl PARTICULAR PURPOSE.

dnl Copyright (c) 1998 N. D. Bellamy

AC_DEFUN(AC_PATH_QT_LIB,
[
  AC_REQUIRE_CPP()
  AC_REQUIRE([AC_PATH_X])
  AC_MSG_CHECKING(for QT libraries)
  
  ac_qt_libraries="no"
  
  AC_ARG_WITH(qt-libraries,
    [  --with-qt-libraries     where the QT libraries are located. ],
    [  ac_qt_libraries="$withval" ])

  AC_CACHE_VAL(ac_cv_lib_qtlib, [
    
    dnl Did the user give --with-qt-libraries?
    
    if test "$ac_qt_libraries" = no; then

      dnl No they didn't, so lets look for them...
    
      qt_libfiles="libqt.so libqt2.so"

      dnl If you need to add extra directories to check, add them here.
      
      qt_library_dirs="\
        /usr/lib \
        /usr/local/lib \
        /usr/lib/qt \
        /usr/lib/qt/lib \
        /usr/local/lib/qt \
        /usr/local/qt/lib \
        /usr/X11/lib \
        /usr/X11/lib/qt \
        /usr/X11R6/lib \
	/usr/X11R6/lib/qt"
  
      if test "x$QTDIR" != x; then
        qt_library_dirs="$qt_library_dirs $QTDIR/lib"
      fi
  
      if test "x$QTLIB" != x; then
        qt_library_dirs="$qt_library_dirs $QTLIB"
      fi
    
      for qt_dir in $qt_library_dirs; do
        for qt_libfile in $qt_libfiles; do
          for qt_check_lib in $qt_dir/$qt_libfile*; do
            if test -r $qt_check_lib; then
              ac_qt_libraries=$qt_dir
              break 2
            fi
          done
        done
      done
    fi

    ac_cv_lib_qtlib=$ac_qt_libraries
  ])

  dnl Define a shell variable for later checks

  if test "$ac_cv_lib_qtlib" = no; then
    have_qt_lib="no"
  else
    have_qt_lib="yes"
  fi
  
  AC_MSG_RESULT([$ac_cv_lib_qtlib])
  QT_LDFLAGS="-L$ac_cv_lib_qtlib"
  QT_LIBDIR="$ac_cv_lib_qtlib"
  AC_SUBST(QT_LDFLAGS)
  AC_SUBST(QT_LIBDIR)
])

AC_DEFUN(AC_PATH_QT_INC,
[
  AC_REQUIRE_CPP()
  AC_REQUIRE([AC_PATH_X])
  AC_MSG_CHECKING(for QT includes)
  
  ac_qt_includes="no"
  
  AC_ARG_WITH(qt-includes,
    [  --with-qt-includes      where the QT headers are located. ],
    [  ac_qt_includes="$withval" ])
  
  AC_CACHE_VAL(ac_cv_header_qtlib, [
    
    dnl Did the user give --with-qt-includes?
    
    if test "$ac_qt_includes" = no; then

      dnl No they didn't, so lets look for them...

      dnl If you need to add extra directories to check, add them here.
      
      qt_include_dirs="\
        /usr/lib/qt/include \
        /usr/include/qt \
        /usr/local/qt/include \
        /usr/local/include/qt \
        /usr/X11/include/qt \
        /usr/X11/include/X11/qt \
	/usr/X11R6/include \        
	/usr/X11R6/include/qt \
	/usr/X11R6/include/qt2 \
        /usr/X11R6/include/X11/qt"

      if test "x$QTDIR" != x; then
        qt_include_dirs="$qt_include_dirs $QTDIR/include"
      fi

      if test "x$QTINC" != x; then
        qt_include_dirs="$qt_include_dirs $QTINC"
      fi

      for qt_dir in $qt_include_dirs; do
        if test -r "$qt_dir/qt.h"; then
          ac_qt_includes=$qt_dir
          break
        fi
      done
    fi

    ac_cv_header_qtlib=$ac_qt_includes
  
  ])

  if test "$ac_cv_header_qtlib" = no; then
    have_qt_inc="no"
  else
    have_qt_inc="yes"
  fi

  AC_MSG_RESULT([$ac_cv_header_qtlib])
  QT_INCLUDES="-I$ac_cv_header_qtlib"
  QT_INCDIR="$ac_cv_header_qtlib"
  AC_SUBST(QT_INCLUDES)
  AC_SUBST(QT_INCDIR)
])

AC_DEFUN(AC_PATH_QT_MOC,
[
  AC_PATH_PROG(
    QT_MOC,
    moc,
    /usr/X11R6/bin/moc2,
    $PATH:/usr/bin:/usr/X11R6/bin:$QTDIR/bin:/usr/lib/qt/bin:/usr/local/qt/bin)
])


dnl Like AC_CHECK_HEADER, but it uses the already-computed -I directories.

AC_DEFUN(AC_CHECK_X_HEADER, [
  ac_save_CPPFLAGS="$CPPFLAGS"
  if test \! -z "$includedir" ; then
    CPPFLAGS="$CPPFLAGS -I$includedir"
  fi
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"
  AC_CHECK_HEADER([$1])
  CPPFLAGS="$ac_save_CPPFLAGS"
])
  
dnl Like AC_CHECK_LIB, but it used the -L dirs set up by the X checks.

AC_DEFUN(AC_CHECK_X_LIB, [
  ac_save_CPPFLAGS="$CPPFLAGS"
  ac_save_LDFLAGS="$LDFLAGS"

  if test \! -z "$includedir" ; then
    CPPFLAGS="$CPPFLAGS -I$includedir"
  fi
  
  dnl note: $X_CFLAGS includes $x_includes
  CPPFLAGS="$CPPFLAGS $X_CFLAGS"

  if test \! -z "$libdir" ; then
    LDFLAGS="$LDFLAGS -L$libdir"
  fi
  
  dnl note: $X_LIBS includes $x_libraries
  
  LDFLAGS="$LDFLAGS $X_LIBS"
  AC_CHECK_LIB([$1], [$2], [$3], [$4], [$5])
  CPPFLAGS="$ac_save_CPPFLAGS"
  LDFLAGS="$ac_save_LDFLAGS"]
)

dnl Check if it is possible to turn off run time type information (RTTI)
AC_DEFUN(AC_PROG_CXX_FNO_RTTI,
[AC_CACHE_CHECK(whether ${CXX-g++} accepts -fno-rtti, ac_cv_prog_cxx_fno_rtti,
[echo 'void f(){}' > conftest.cc
if test -z "`${CXX-g++} -fno-rtti -c conftest.cc 2>&1`"; then
  ac_cv_prog_cxx_fno_rtti=yes
  CXXFLAGS="${CXXFLAGS} -fno-rtti"
else
  ac_cv_prog_cxx_fno_rtti=no
fi
rm -f conftest*
])])


