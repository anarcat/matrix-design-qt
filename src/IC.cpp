/****************************-*-c++-*-*********************************
 * $Id: IC.cpp,v 1.6 1999/09/17 03:00:06 spidey Exp spidey $
 **********************************************************************
 * The IC definition
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: IC.cpp,v $
 * Revision 1.6  1999/09/17 03:00:06  spidey
 * Debugged some null pointers
 * Random rating setting in random constructor
 *
 * Revision 1.5  1999/07/22 13:25:12  spidey
 * Serialisation on QTextStream
 *
 * Revision 1.4  1999/06/08 15:00:25  spidey
 * See .h
 *
 * Revision 1.3  1999/06/01 19:48:33  spidey
 * Serialisation
 *
 * Revision 1.2  1999/06/01 17:04:23  spidey
 * Comments
 *
 * Revision 1.1  1999/05/28 17:57:39  spidey
 * Initial revision
 *
 *********************************************************************/

#include "IC.h"
#include "Node.h"
#include <stdlib.h>

IC::IC() {

  type = rating = 0;

}

/**
 * Create a random IC in a node of color 'code'
 */
IC::IC(int code) {

  int low = 0, high = 0;
  switch (code) {
  case BLUE:
    delete this;
    return;
  case GREEN:
    low = 8;
    high = 11;
    break;
  case ORANGE:
    low = 7;
    high = 10;
    break;
  case RED:
    low = 6;
    high = 10;
    break;
  }
  low-=2;
  high-=2;
  int dice = random() %10;
  if (dice <= low) { // this is white IC
    setRType(WHITE);
  } else if (dice <= high) { // this is gray IC
    setRType(GRAY);
  } else
    setType(BLACK); // this is ... BLACK IC! <eek!>
  setRating();
  
}

/**
 * Copy constructor
 */
IC::IC(IC *ic) {

  type = ic->getType();
  rating = ic->getRating();

}

/**
 * Create a node with the given fields
 */
IC::IC(int t, int r) {

  type = t;
  rating = r;

}

/**
 * Returns the appropriate fields
 */
int IC::getType() const {return type;}
int IC::getRating() const {return rating;}
  
/**
 * Returns the appropriate text for the color or type
 */
inline const QString IC::getColorText() {return getColorText(type);}

const QString IC::getColorText(int type) {

  if (type < BLASTER) return "White";
  else if (type < BLACK) return "Gray";
  else return "Black";

}

const QString IC::getTypeText() const {return getTypeText(type); }

const QString IC::getTypeText(int type) {
  
  switch (type) {
  case ACCESS:
    return "Access";
    break;
  case BARRIER:
    return "Barrier";
    break;
  case SCRAMBLE:
    return "Scramble";
    break;
  case BLASTER:
    return "Blaster";
    break;
  case KILLER:
    return "Killer";
    break;
  case TAR_BABY:
    return "Tar Baby";
    break;
  case TAR_PIT:
    return "Tar Pit";
    break;
  case TRACE:
    return "Trace";
    break;
  case TRACE_REPORT:
    return "Trace & Report";
    break;
  case TRACE_DUMP:
    return "Trace & Dump";
    break;
  case TRACE_BURN:
    return "Trace & Burn";
    break;
  case BLACK:
    return "Black";
    break;
  default:
    return "????";
    break;
  } // switch type

  
}

/**
 * Set a random type with mintype as a minimum
 */
void IC::setRType(int minType) {

  // the dice roll
  int dice = 0;
  // mintype determines if this is a white, gray or black ic
  if (minType < GRAY) {
    dice = random() % 10;
    if (dice == 0 || dice == 10) { // trapped IC
      // roll again to determine what will be the type
      dice = random() % 8;
      if (dice <= 4) setType(ACCESS);
      else if (dice <= 6) setType(BARRIER);
      else setType(SCRAMBLE);
      // to tell we must add another
      setRating(TRAPPED);
    } else if (dice <= 5)
      setType(ACCESS);
    else if (dice <= 7)
      setType(BARRIER);
    else if (dice <= 9)
      setType(SCRAMBLE);
  } else if (minType < BLACK) {
    // this is a gray IC
    dice = random() % 10;
    if (dice <= 2) setType(BLASTER);
    else if (dice <= 4) setType(TRACE);
    else if (dice <= 6) setType(KILLER);
    else if (dice <= 9) setType(TAR_BABY);
    else setType(TAR_PIT);
  } else setType(BLACK); // black IC
  
}

/**
 * Set the type
 */
void IC::setType(int t) {

  type = t;
  if (t < 0) type = 0;
  if (t > 11) type = 11;

  emit chgType(type);
}

/**
 * Set a rating at random
 */
void IC::setRating() { setRating(random() % 10 +1); }

/**
 * set the rating
 */
void IC::setRating(int r) {
  rating = r;
  emit chgRating(rating);
}

QTextStream &operator<< ( QTextStream &s, const IC &i) {

  return s << i.getTypeText() << " - " << i.getRating();

}

QDataStream &operator<< ( QDataStream &s, const IC &i) {

  return s << (INT16) i.getType() << (INT16) i.getRating();

}

QDataStream &operator>> ( QDataStream &s, IC &i) {

  INT16 type, rating;
  s >> type >> rating;
  if (&i != 0) i.setType(type); i.setRating(rating);
  return s;

}
