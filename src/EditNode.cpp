/****************************-*-c++-*-*********************************
 * $Id: EditNode.cpp,v 0.5 1999/09/17 02:56:21 spidey Exp spidey $
 **********************************************************************
 * QTabDialog Implementation to edit Node objects
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: EditNode.cpp,v $
 * Revision 0.5  1999/09/17 02:56:21  spidey
 * Added the 4th panel: data editing
 *
 * Revision 0.4  1999/07/23 02:30:27  spidey
 * Added IC modification! (second tab)
 * + addIC/delIC/modIC functions
 *
 * Revision 0.3  1999/07/22 01:22:12  spidey
 * Complete rewrite.
 * Single tab with color, rating and description, for now
 *
 * Revision 0.2  1999/06/08 14:31:03  spidey
 * Added IC and Description GUI layouts. No use yet.
 *
 * Revision 0.1  1999/05/28 17:58:46  spidey
 * *** empty log message ***
 *
 *********************************************************************/

#include "EditNode.h"
#include <qspinbox.h>
#include <qcombobox.h>
#include <qlayout.h>
#include <qlabel.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qpushbutton.h>
#include <qlineedit.h>

#ifdef DEBUG
#include <iostream.h>
#endif

EditNode::EditNode(Node *o)
: QTabDialog(0,"EditNodeDialog", FALSE,0)  {

  original = o;
  editedNode = new Node(o);
  editedNode->hide();

  /*****************************************************************************
   * First tab: general stats
   ****************************************************************************/
  QWidget *w = new QWidget(this, "stats");

  QGridLayout *layout = new QGridLayout(w, 3, 3, 3);

  // The button group
  QButtonGroup *grp = new QButtonGroup( "Color", w, "Button group");

  // the button group layout
  QVBoxLayout *vbox = new QVBoxLayout(grp, 3);

  vbox->addSpacing( grp->fontMetrics().height() );

  // Setup the 4 buttons
  QRadioButton *button = new QRadioButton( grp, "Blue" );
  button->setText("Blue");
  button->setFixedSize( button->sizeHint() );
  //button->setMaximumSize( 500, button->minimumSize().height() );

  vbox->addWidget( button, 0, AlignLeft );

  button = new QRadioButton( grp, "Green" );
  button->setText("Green");
  button->setFixedSize( button->sizeHint() );
  //button->setMaximumSize( 500, button->minimumSize().height() );
  
  vbox->addWidget( button, 0, AlignLeft );
  
  button = new QRadioButton( grp, "Yellow" );
  button->setText("Yellow");
  button->setFixedSize( button->sizeHint() );
  //button->setMaximumSize( 500, button->minimumSize().height() );
  
  vbox->addWidget( button, 0, AlignLeft );

  button = new QRadioButton( grp, "Red" );
  button->setText("Red");
  button->setFixedSize( button->sizeHint() );
  
  vbox->addWidget( button, 0, AlignLeft );

  // size the button group appropriatly
  grp->setFixedSize( grp->childrenRect().size() );

  // Rating
  QSpinBox *rating = new QSpinBox(w,"rating field");
  rating->setFixedSize(QSize(40,20));
  //  rating->setMaximumSize(QSize(40,20));
  rating->show();
  QLabel *ratingl = new QLabel(rating, "&Rating: ",w, "statsLabel", 0);
  ratingl->show();
  
  description = new QMultiLineEdit(w, "description");
  QLabel *commL = new QLabel(description, "Additionnal &description", w, 0, 0);

  layout->addMultiCellWidget(commL,1,1,1,2);
  layout->addMultiCellWidget(description,2,2,1,2);

  layout->addMultiCellWidget(grp,0,2,0,0);
  layout->addWidget(ratingl,0,1);
  layout->addWidget(rating,0,2);
  
  grp->setButton(editedNode->getColor());
  rating->setValue(editedNode->getRating());

  description->setText(editedNode->getDesc());
  
  vbox->activate();
  layout->activate();

  addTab(w, "&Statistics");

  /*****************************************************************************
   * Second tab: ICes
   ****************************************************************************/
  w = new QWidget(this, "ices");

  layout = new QGridLayout(w, 3, 2, 3);
  
  icList = new QListBox(w, "ic list");
  icList->setMinimumSize(QSize(100, 100));
  icList->setMaximumSize(QSize(2000, 2000));
  
  QPushButton
    *add = new QPushButton("Add", w, "add button"),
    *del = new QPushButton("Delete", w, "delete button"),
    *mod = new QPushButton("Modify", w, "modify button");

  add->setFixedSize(add->sizeHint());
  del->setFixedSize(del->sizeHint());
  mod->setFixedSize(mod->sizeHint());
  
  updateIClist();

  layout->addMultiCellWidget(icList, 0, 2, 0, 0);
  layout->addWidget(add, 0, 1);
  layout->addWidget(del, 1, 1);
  layout->addWidget(mod, 2, 1);

  layout->activate();
  
  connect(add, SIGNAL(released()),
	  this, SLOT(addIC()));
  connect(del, SIGNAL(released()),
	  this, SLOT(delIC()));
  connect(mod, SIGNAL(released()),
	  this, SLOT(modIC()));

  addTab(w, "&IC");

  /*****************************************************************************
   * Third tab: Datafiles
   ****************************************************************************/
  w = new QWidget(this, "data");

  layout = new QGridLayout(w, 3, 2, 3);
  dataList = new QListBox(w, "list view of data files");
  updateDataList();
   
  add = new QPushButton("Add", w, "add button");
  del = new QPushButton("Delete", w, "delete button");
  mod = new QPushButton("Modify", w, "modify button");

  add->setFixedSize(add->sizeHint());
  del->setFixedSize(del->sizeHint());
  mod->setFixedSize(mod->sizeHint());

  layout->addMultiCellWidget(dataList, 0, 2, 0, 0);
  layout->addWidget(add, 0, 1);
  layout->addWidget(del, 1, 1);
  layout->addWidget(mod, 2, 1);

  connect(add, SIGNAL(released()),
	  this, SLOT(addData()));
  connect(del, SIGNAL(released()),
	  this, SLOT(delData()));
  connect(mod, SIGNAL(released()),
	  this, SLOT(modData()));

  addTab(w, "&Datafiles");
  
  /*****************************************************************************
   * Final setup
   ****************************************************************************/
  // Set the title
  setCaption( "Edit Node Statistics" );

  // We want the Random, OK, and Cancel buttons.
  //setDefaultButton("Random");
  setOkButton("OK");
  setApplyButton("Apply");
  setCancelButton("Cancel");
  
  // What do the buttons do:
  connect(this, SIGNAL(applyButtonPressed()),
	  this, SLOT(apply()));

  connect (rating, SIGNAL(valueChanged(int)),
           editedNode, SLOT(setRating(int)));
  connect (grp, SIGNAL(clicked(int)),
           editedNode, SLOT(setColor(int)));
  show();

}

EditNode::~EditNode() {}

void EditNode::apply() {

  editedNode->setDesc(description->text());
  original->setNode(*editedNode);

}

void EditNode::addIC(IC *ic) {

  // a popup prompt
  QDialog *prompt = new QDialog(0, "AdddIC Prompt", TRUE);
  // ... with a grid layout
  QGridLayout *lay = new QGridLayout(prompt, 2, 2, 3);
  
  // ok/cancel buttons
  QPushButton *ok, *cancel;
  ok = new QPushButton( "Ok", prompt );
  cancel = new QPushButton( "Cancel", prompt );

  // IC type 'submenu'
  QComboBox *types = new QComboBox(false, prompt, "type list");
  // and its content
  for (int i = 0; i < BLACK; i++)
    types->insertItem(IC::getTypeText(i));
  
  // IC rating
  QSpinBox *rating = new QSpinBox(1, 128, 1, prompt, "rating box");

  if (ic == 0) 
    prompt->setCaption("Add IC");
  else {
    types->setCurrentItem(ic->getType());
    rating->setValue(ic->getRating());
    prompt->setCaption("Modify IC");
  }
  
  // setup item sizes
  cancel->setMinimumSize(cancel->sizeHint());
  cancel->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  ok->setMinimumSize(ok->sizeHint());
  ok->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  rating->setFixedSize(rating->sizeHint());
  types->setFixedSize(types->sizeHint());

  // throw them in a layout
  lay->addWidget(types, 0, 0);
  lay->addWidget(rating, 0, 1);
  lay->addWidget(ok, 1, 0 );
  lay->addWidget(cancel, 1, 1);
  lay->activate();
  
  // few connections to add or not
  connect( ok, SIGNAL(clicked()),
	   prompt, SLOT(accept()) );
  connect( cancel, SIGNAL(clicked()),
	   prompt, SLOT(reject()) );


  // this is a modal dialog, send it the flow of control
  if (prompt->exec())
    // ok was pressed
    if (ic == 0)
      editedNode->addIC(*new IC(types->currentItem(), rating->value()));
    else {
      ic->setType(types->currentItem());
      ic->setRating(rating->value());
    }
  
  // take account of the changes
  updateIClist();
  
}

void EditNode::delIC() {
  if (int tmp = icList->currentItem() > 0) {
    icList->removeItem(tmp-1);
    editedNode->removeIC(tmp-1);
  }  
}

void EditNode::modIC() {

  int tmp = icList->currentItem();
  if (tmp != -1) {
    QList<IC> ices = editedNode->getICsList();
    addIC(ices.at(tmp));
  }

}

void EditNode::addData(Data *data) {

  // a popup prompt
  QDialog *prompt = new QDialog(0, "Data Prompt", TRUE);
  // ... with a grid layout
  QGridLayout *lay = new QGridLayout(prompt, 4, 2, 3);
  
  // Data name
  QLineEdit *filename = new QLineEdit(prompt, "filename field");
  // Data size
  QSpinBox *size = new QSpinBox(1, 1000000, 1, prompt, "size box");
  // Data value
  QSpinBox *value = new QSpinBox(0, 1000000, 1, prompt, "value box");
  // Data description
  QLineEdit *desc = new QLineEdit(prompt, "description field");

  // ok/cancel buttons
  QPushButton *ok, *cancel;
  ok = new QPushButton( "Ok", prompt );
  cancel = new QPushButton( "Cancel", prompt );

  if (data == 0) 
    prompt->setCaption("Add Data");
  else {
    filename->setText(data->getFilename());
    size->setValue(data->getSize());
    value->setValue(data->getValue());
    desc->setText(data->getDesc());
    prompt->setCaption("Modify Data");
  }
  
  // setup item sizes
  cancel->setMinimumSize(cancel->sizeHint());
  cancel->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  ok->setMinimumSize(ok->sizeHint());
  ok->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  filename->setMinimumSize(filename->sizeHint());
  size->setFixedSize(size->sizeHint());
  value->setFixedSize(value->sizeHint());
  desc->setMinimumSize(desc->sizeHint());

  // throw them in a layout
  lay->addWidget(filename, 0, 0);
  lay->addWidget(size, 2, 0);
  lay->addWidget(value, 2, 1);
  lay->addWidget(desc, 1, 0);
  lay->addWidget(ok, 3, 0 );
  lay->addWidget(cancel, 3, 1);
  lay->activate();
  
  // few connections to add or not
  connect( ok, SIGNAL(clicked()),
	   prompt, SLOT(accept()) );
  connect( cancel, SIGNAL(clicked()),
	   prompt, SLOT(reject()) );


  // this is a modal dialog, send it the flow of control
  if (prompt->exec())
    // ok was pressed
    if (data == 0)
      editedNode->addData
	(*new Data(filename->text(), size->value(),
		   value->value(), desc->text() ));
    else {
      data->setFilename(filename->text());
      data->setValue(value->value());
      data->setSize(size->value());
      data->setDesc(desc->text());
    }
  
  // take account of the changes
  updateDataList();

}

void EditNode::delData() {
  int tmp = dataList->currentItem();
  if (tmp >= 0) {
    dataList->removeItem(tmp);
    editedNode->removeData(tmp);
  }
}

void EditNode::modData() {
  int tmp = dataList->currentItem();
  if (tmp >= 0) {
    QList<Data> data = editedNode->getDataList();
    addData(data.at(tmp));
  }
}

void EditNode::updateIClist() {

  icList->clear();
  QListIterator<IC> ices(editedNode->getICsList());
  ices.toFirst();

  // char *txt = 0;
  for (; ices.current(); ++ices) {
    // txt = new char[20];
    // sprintf(txt, "%s - %d",ices.current()->getTypeText() , ices.current()->getRating());
    icList->insertItem
      (ices.current()->getTypeText() +
       tr(" - %1").arg(ices.current()->getRating()));
       //delete txt;
  }

}

void EditNode::updateDataList() {

  dataList->clear();
  QListIterator<Data> data(editedNode->getDataList());
  data.toFirst();

  Data *cur = 0;
  //char *txt = 0;
  for (; data.current(); ++data) {
    cur = data.current();
    // txt = new char[1024];
    // sprintf(txt, "%s: %d Mp, %d Nuyen (%s)", cur->getFilename(), cur->getValue(),
    //         cur->getSize(), cur->getDesc());
    dataList->insertItem(cur->getShortText());
    //delete txt;
  }

}
