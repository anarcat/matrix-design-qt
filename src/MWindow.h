/****************************-*-c++-*-*********************************
 * $Id: MWindow.h,v 0.7 1999/08/26 23:32:22 spidey Exp spidey $
 **********************************************************************
 * The Main window
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: MWindow.h,v $
 * Revision 0.7  1999/08/26 23:32:22  spidey
 * Added options menu with global color submenu
 * Added saveImage() slot that will save the matrix as an image
 * Added intialize() function that will construct various parts of the
 * window
 *
 * Revision 0.6  1999/07/23 22:24:35  spidey
 * Added modified() slot
 * Added the fileMenu as a private field
 *
 * Revision 0.5  1999/07/22 13:23:43  spidey
 * Implemented save as
 *
 * Revision 0.4  1999/06/07 15:55:02  spidey
 * + MWindow(QDatastream &) (reads a Matrix from Datastream)
 *
 * Revision 0.3  1999/05/28 18:07:33  spidey
 * *** empty log message ***
 *
 * Revision 0.2  1999/05/25 16:18:00  spidey
 * First working version
 *
 *********************************************************************/

#ifndef MAINW_H
#define MAINW_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qmainwindow.h>
#include <qfiledialog.h>
#include "ToolBar.h"

class QPopupMenu;

class MWindow : public QMainWindow {
  
  Q_OBJECT

  Matrix *matrix;
  
public:
  MWindow();
  MWindow(QDataStream &stream);
  ~MWindow();

public slots:
  void modified();

private slots:
  void newWindow();
  void closeWindow();
  void load();
  inline void save() {
    if (matrix->isModified()) {
      if (fileName.isEmpty())
	fileName = QFileDialog::getSaveFileName( 0 );
      real_save();
    } else {
      // signal that there's nothing to to...
    }
  }
  
  inline void saveAs() {
    matrix->modify();
    fileName = "";
    save();
  }

  void saveImage();
  void saveReport();
  
  void print();

private:
  QPopupMenu *fileMenu, *colorMenu, *optionMenu;
  QString fileName;
  void real_save();
  void initialize();

}; // class Node

#endif
