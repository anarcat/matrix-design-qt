#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qapplication.h>
#include <qpushbutton.h>
#include <qpopupmenu.h>
#include <qmenubar.h>
#include <stdlib.h>

#ifdef DEBUG
#include <iostream.h>
#endif

#include "MWindow.h"
#include "ToolBar.h"

int main( int argc, char **argv )
{

  srandomdev();
  
  QApplication a( argc, argv );

  a.setDoubleClickInterval(200);
  
  toolbar = new ToolBar();

  MWindow * mw = new MWindow();
  mw->show();
  
  a.connect( &a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()) );

  return a.exec();

}
