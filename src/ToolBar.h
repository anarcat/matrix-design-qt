/****************************-*-c++-*-*********************************
 * $Id: ToolBar.h,v 0.3 1999/06/07 15:56:29 spidey Exp spidey $
 **********************************************************************
 * ToolBar
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: ToolBar.h,v $
 * Revision 0.3  1999/06/07 15:56:29  spidey
 * Modified very little...
 *
 * Revision 0.2  1999/05/26 16:26:57  spidey
 * Added necessary slots/signals to choose the next node
 *
 * Revision 0.1  1999/05/26 08:34:09  spidey
 * Implemented few slots (destroy()...)
 *********************************************************************/

#ifndef TOOLBAR_H
#define TOOLBAR_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qwidget.h>
#include "Matrix.h"
#include "Node.h"

class ToolBar;
extern Q_EXPORT ToolBar *toolbar;

class ToolBar : public QWidget {

  Q_OBJECT
  
public:
  ToolBar();
  ~ToolBar();
 
signals:
  void curTypeChg(int);
  void curColorChg(int);
  void curRatingChg(int);
  
};

#endif
