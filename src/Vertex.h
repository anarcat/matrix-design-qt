/****************************-*-c++-*-*********************************
 * $Id: Vertex.h,v 0.3 1999/06/01 01:45:15 spidey Exp spidey $
 **********************************************************************
 * The Vertex: the dataline that links node
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Vertex.h,v $
 * Revision 0.3  1999/06/01 01:45:15  spidey
 * This is an QOjbect again!
 * features a del() slot and deleting() signal
 *
 * Revision 0.2  1999/05/31 14:42:27  spidey
 * This is not an qobject anymore. It only exists as a list in Matrix
 *
 * Revision 0.1  1999/05/29 18:06:24  spidey
 * *** empty log message ***
 *
 *********************************************************************/

#ifndef VERTEX_H
#define VERTEX_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qobject.h>
#include "Node.h"

class Vertex  : public QObject {

  Q_OBJECT
  
private:
  bool selection;
  Node *from;
  Node *to;
  QRect *oldRect;
  
public:
  Vertex(/*QWidget *parent,*/ Node*, Node*);

  QPointArray getPoints();

  QPoint getPointA();
  QPoint getPointB();

  int getNumA() {return from->getNumber();}
  int getNumB() {return to->getNumber();}
  
  QRect getOldRect() {
    QRect ol(*oldRect);
    delete oldRect;
    oldRect = new QRect(from->getCenter(), to->getCenter());
    return ol;
  }

  // a little too much to being with...
  //Vertex(Node*, QPoint*);
  
//    bool event(QEvent*);
  
public slots:

  void del();
  
//   void select();
//   void unselect();
  
signals:
  void deleting(Vertex*);

//   void selected(Vertex*);
//   void unselected(Vertex*);
//   void deselect();
  
};

#endif
