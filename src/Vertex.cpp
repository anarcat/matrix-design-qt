/****************************-*-c++-*-*********************************
 * $Id: Vertex.cpp,v 0.2 1999/05/31 14:42:43 spidey Exp spidey $
 **********************************************************************
 * The Vertex: the dataline that links node
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Vertex.cpp,v $
 * Revision 0.2  1999/05/31 14:42:43  spidey
 * see vertex.h
 *
 * Revision 1.1  1999/05/29 18:04:49  spidey
 * QObject Vertex
 *
 *********************************************************************/

#include "Vertex.h"
#include <qpointarray.h>
#ifdef DEBUG
#include <iostream.h>
#endif

Vertex::Vertex(Node *f, Node *t) {
  //  : QObject(parent, "vertex") {

  from = f;
  to = t;
  oldRect = new QRect(from->getCenter(), to->getCenter());
  
}

QPointArray Vertex::getPoints() {

  QPointArray qpa;
  qpa.putPoints(4,
                from->x(), from->y(),
                to->x(), to->y());
  
  cerr << "getting points from vertex " << this << endl;
  return qpa;
}

QPoint Vertex::getPointA() {

  return QPoint((from->getCenter()));

}

QPoint Vertex::getPointB() {

  return QPoint((to->getCenter()));

}

void Vertex::del() {

  emit deleting(this);

}

// This handles the selection and painting
// bool Vertex::event(QEvent*) {
//   return FALSE;
// }
  
// void Vertex::select() {}
// void Vertex::unselect() {}
