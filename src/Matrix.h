/****************************-*-c++-*-*********************************
 * $Id: Matrix.h,v 0.12 1999/08/24 19:41:12 spidey Exp spidey $
 **********************************************************************
 * Matrix Widget that is the Matrix itself...
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Matrix.h,v $
 * Revision 0.12  1999/08/24 19:41:12  spidey
 * + Added the global Color field, and associated functions
 * + Added recursive Randomize()
 * + made a few functions inline
 *
 * Revision 0.11  1999/07/23 11:49:19  spidey
 * Now takes account of the 'one-CPU' rule.
 * Added randomize() slot which creates random nodes
 * Added modified() signal
 *
 * Revision 0.10  1999/07/22 13:33:33  spidey
 * Added 'modified' field and associated functions
 * QTextStream writing
 *
 * Revision 0.9  1999/07/20 01:38:35  spidey
 * Added methods to access various nodes
 *
 * Revision 0.8  1999/06/07 15:52:26  spidey
 * + Comments
 * + getVertices
 * + addNode(Node*)
 * + addVertex(QPoint*, QPoint*)
 * + oper >> <<
 *
 * Revision 0.7  1999/06/01 17:54:14  spidey
 * Comments
 *
 * Revision 0.6  1999/06/01 01:42:49  spidey
 * + RemoveNode(Node*)
 * + RemoveVertex(Vertex*) slots
 *
 * Revision 0.5  1999/06/01 00:33:15  spidey
 * patch up check in to catch up on .cpp
 *
 * Revision 0.4  1999/06/01 00:27:22  spidey
 * An event filter now handles vertex creation:
 * + eventFilter()
 * + addNodeToVertex()
 * + ping(QPoint) to check if there's a node at point
 *   Connected to addNodeToVertex()
 *
 * Revision 0.3  1999/05/31 14:46:22  spidey
 * Added the vertex list and necessary slots/signals
 * Most functions now use &'s instead of *'s..
 *
 * Revision 0.2  1999/05/28 18:05:40  spidey
 * A lot of modifs... too much tomention here...
 *********************************************************************/

#ifndef MATRIX_H
#define MATRIX_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qwidget.h>
#include <qdatastream.h>
#include "Vertex.h"
#include "Node.h"

/**
 * Matrix Stream functions
 */

class Matrix : public QWidget {

  Q_OBJECT
  
private:

  // if the matrix have been modified
  bool modif;
  // if the matrix has already a CPU
  bool hasCPU;
  // global color rating
  int globalColor;
  // the next node stats:
  int color, rating, type;
  // this is the node from which we drag
  Node *fromNode;
  // the list of selected nodes
  QList<Node> *selected;
  // the list of vertices
  QList<Vertex> *vertices;
  // the node list
  QList<Node> *nodeList;
  
  
public:
  // creates a matrix with parent
  Matrix(QWidget *parent);
  // destructor
  ~Matrix();
  // tells if the matrix have been modified
  inline bool isModified() {return modif;}
  
protected:
  
  void mouseDoubleClickEvent ( QMouseEvent * );
  void mouseReleaseEvent( QMouseEvent* );
  void paintEvent( QPaintEvent * );
  bool eventFilter( QObject *, QEvent * );

public:
  inline QList<Vertex> * getVertices() const { return vertices; }
  QPixmap getPixmap();
  inline int nodeCount() const { return nodeList->count(); }
  inline Node *nodeAt(int i) const { return nodeList->at(i); }
  inline int getGlobalColor() {return globalColor;}
  
public slots:

  // creates random nodes with a starting SAN
  void randomize();
  // create random nodes from a starting node
  void randomize(Node *, int, int);

  // modify routines
  inline void unModify() { modif = false; }
  inline void modify() { modif = true; emit modified();}

  // These set the fields of the added node
  inline void setType(int t) {type = t;}
  inline void setColor(int c) {color = c;}
  inline void setRating(int r) {rating = r;}

  // set the global color of the Matrix
  inline void setGlobalColor(int g) {globalColor = g;}
  
  // add a node to the selection
  void addSelected(Node *);
  // remove a node from the selection
  void removeSelected(Node *);
  // remove all node from selection
  void unselect();
  
  // add node NextNode at point QPoint
  bool addNode(const QPoint &);
  void addNode(Node *n);
  // remove given Node
  void removeNode(Node *);
  
  // add a vertex with the given nodes as extremities
  void addVertex(Node *, Node *);
  // add a vertex with 2 probes at 2 points
  void addVertex(const QPoint &, const QPoint&);
  // add a node to the vertex in creation
  void addNodeToVertex(Node *);
  // remove the given vertex
  void removeVertex(Vertex *);

signals:

  // tell everybody we're trying a vertex at QPoint*
  void ping(const QPoint &);
  // the selection have been moved by diff
  void moved(const QPoint &);
  // the matrix have been modified!
  void modified();
  
};

Q_EXPORT QTextStream &operator<<( QTextStream &, const Matrix & );
Q_EXPORT QDataStream &operator<<( QDataStream &, const Matrix & );
Q_EXPORT QDataStream &operator>>( QDataStream &, Matrix & );

#endif
