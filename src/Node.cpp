/****************************-*-c++-*-*********************************
 * $Id: Node.cpp,v 0.15 1999/08/26 22:46:15 spidey Exp spidey $
 **********************************************************************
 * Node definition
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Node.cpp,v $
 * Revision 0.15  1999/08/26 22:46:15  spidey
 * Implemented some kind of selection highlighting (flickers!)
 *
 * Revision 0.14  1999/08/26 20:15:37  spidey
 * Implemented QBitmap masks!!!
 * Commented traceNode(...) in order if it can be deleted
 * Removed shape tracing in paintEvent (the mask takes care of this
 * now...)
 *
 * Revision 0.13  1999/08/24 19:29:33  spidey
 * + Improved copy constructor and copy procedure (setNode())
 * + Shrinked the DS and SN sizes
 * + Type now defaults to SPU
 * + Randomize() now takes a int color arg
 * + Cleaned a little of paintEvent()
 * + Serialization should work great now
 *
 * Revision 0.12  1999/07/22 14:28:56  spidey
 * Added: 	- Connections to modified() (any modification signaled)
 * 	- node numbering facilities
 * 	- QTextStream writing
 * Removed: debug messages
 *
 * Revision 0.11  1999/07/22 01:14:51  spidey
 * Added description field and consequent fonctions and modifications
 *
 * Revision 0.10  1999/07/22 00:40:26  spidey
 * Removed random calls from constructors
 * Simplified rating and type assignement
 *
 * Revision 0.9  1999/06/01 20:29:20  spidey
 * Serialisation
 *
 * Revision 0.8  1999/06/01 19:05:45  spidey
 * Comments
 *
 * Revision 0.7  1999/06/01 01:43:31  spidey
 * delete key now sends the 'deleting(this)' signal
 *
 * Revision 0.6  1999/06/01 00:38:56  spidey
 * see node.h...
 *
 * Revision 0.5  1999/05/31 20:57:39  spidey
 * - Treats key codes and receives keyboard focus.
 * - Added ping(qpoint) slot
 * - Implemented basic vertex drawing, that should, BTW, be handled in
 *   Matrix
 *
 * Revision 0.4  1999/05/28 18:04:51  spidey
 * + Clarified the code (seperated cons/des from signals, slots and events)
 * + New moving implementation (initial QPoint recorded and diff'd)
 * + QColor is now a pointer
 * + Random Node Creation implemented with necessary signals/slots
 *
 * Revision 0.3  1999/05/27 04:38:21  spidey
 * Implemented CPU/SPU tracing
 * Added EditNode
 *
 * Revision 0.2  1999/05/26 19:11:43  spidey
 * Added:
 * - Drawing for all except CPU/SPU
 * - Dragging
 * - various set* and get* methods
 *********************************************************************/

#include "Node.h"
#include "EditNode.h"
#include <stdlib.h>
#include <qkeycode.h>
#include <qbitmap.h>
// the bitmaps representing the nodes
#include "spu.bmp"
#include "cpu.bmp"
#include "iop.bmp"
#include "san.bmp"
#include "sn.bmp"
#include "ds.bmp"
// their frame
#include "spuf.bmp"
#include "cpuf.bmp"
#include "iopf.bmp"
#include "sanf.bmp"
#include "snf.bmp"
#include "dsf.bmp"

#ifdef DEBUG
#include <iostream.h>
#endif

// this will be the Square Root of 3 for the time being
#define SQRT3 1.732050808


/******************************************************************************
 * Constructors
 */
// the node is hidden by default
// this constructor should never be used! It should and _will_ be removed
Node::Node() : QWidget(0, "null node") {hide();}

// this is a random node
Node::Node(QWidget *parent)
  : QWidget(parent, "node widget") {

  selection = dragging = false;
  initial = new QPoint(0,0);
  // we can get the focus
  setFocusPolicy(StrongFocus);
  ices = new QList<IC>();
  datalist = new QList<Data>();
  description = "";
  
  connect(this, SIGNAL(chgRating(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgType(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgColor(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(moved(const QPoint &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(deleting(Node *)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgIC()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedIC(IC &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedIC(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(int)),
	  this, SIGNAL(modified()));
 
}

// specific node
Node::Node( QWidget *parent , int t, int c, int r )
  : QWidget(parent, "node widget orig") {

  selection = dragging = false;
  initial = new QPoint(0,0);
  setFocusPolicy(StrongFocus);
  setColor(c);
  setType(t);
  setRating(r);

  // initialize ices and datalist
  ices = new QList<IC>();
  datalist = new QList<Data>();
  description = "";

  connect(this, SIGNAL(chgRating(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgType(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgColor(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(moved(const QPoint &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(deleting(Node *)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgIC()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedIC(IC &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedIC(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(int)),
	  this, SIGNAL(modified()));

  show();

}

// copy constructor
Node::Node(Node *n)
  : QWidget(0, "node widget copy") {

  // initialize everything to the other's stuff
  selection = dragging = false;
  initial = new QPoint(0,0);
  setFocusPolicy(StrongFocus);
  setType(n->getType());
  setColor(n->getColor());
  setRating(n->getRating());

  datalist = new QList<Data>();
  QListIterator<Data> it( n->getDataList() );
  it.toFirst();
  for (; it.current(); ++it)
    datalist->append(it.current());

  ices = new QList<IC>();
  QListIterator<IC> ICit( n->getICsList() );
  ICit.toFirst();
  for (; ICit.current(); ++ICit)
    ices->append(ICit.current());

  setDesc(n->getDesc());
  // this will recreate the node to be about the same widget
  recreate(n->parentWidget(), 0, n->pos(), FALSE);
  if (!n->isVisible())
    hide();

  connect(this, SIGNAL(chgRating(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgType(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgColor(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(moved(const QPoint &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(deleting(Node *)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgIC()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(chgData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedIC(IC &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData()),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(addedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedIC(int)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(Data &)),
	  this, SIGNAL(modified()));
  connect(this, SIGNAL(removedData(int)),
	  this, SIGNAL(modified()));
  
}

// destructor
Node::~Node() {

//   cerr << "======\nobj info:" << endl;
//   dumpObjectInfo ();
//   cerr << "======\nobj tree:" << endl;
//   dumpObjectTree ();
//   cerr << "done" << endl;
//   cerr << "*this Node: " << this;
  delete realColor;
//   cerr << "deleted realcolor, ";
  delete initial;
//   cerr << "initial, ";
  delete datalist;
//   cerr << "datalist, ";
  delete ices;
//   cerr << "ices, done." << endl;

}

/******************************************************************************
 * Slots
 */

/**
 * Make this node selected
 */
void Node::select() {
  selection = true;
  update();
  emit selected(this);
}

/**
 * Unselect this node
 */
void Node::unselect() {
  selection = false;
  update();
  emit unselected(this);
}

/**
 * Move the node by a diff displacement
 */
void Node::dmove(const QPoint &diff) {move(pos() + diff);}

/**
 * Assign another Node to this one
 */
void Node::setNode (Node &n) {
  setColor(n.getColor());
  setRating(n.getRating());
  setType(n.getType());
  setDesc(n.getDesc());

  delete datalist;
  datalist = new QList<Data>();
  QListIterator<Data> it( n.getDataList() );
  it.toFirst();
  for (; it.current(); ++it)
    datalist->append(it.current());
  
  delete ices;
  ices = new QList<IC>();
  QListIterator<IC> ICit( n.getICsList() );
  ICit.toFirst();
  for (; ICit.current(); ++ICit)
    ices->append(ICit.current());
  
  emit modified();
}

/**
 * Set the color node for the node
 */
void Node::setColor(int c) {

  // we take anything greater and lower and the range...
  if (c < BLUE) color = BLUE;
  else if (c > RED) color = RED;
  else  color = c;
  delete realColor;
  realColor = new QColor(getRealColor(c));
  update();
  emit chgColor(c);
  
}

/**
 * Set a random color for this node from a global system color
 */
void Node::setRColor(int global) {

  // either it is the normal or one more/less
  switch (random()%6) {
  case 0:
    setColor(global-1);
    break;
  case 5:
    setColor(global+1);
    break;
  default:
    setColor(global);
    break;
  }
  
}

/**
 * Sets type of node to t
 */
void Node::setType(int t) {

  type = t;
  emit chgType(t);
  // we change the size of the nodes depending on the node type
  // this should be a mask
  QBitmap bm;
  switch (type) {
  case SAN:
    setFixedSize(san_width, san_height);
    bm = QBitmap (san_width, san_height, san_bits, TRUE);
    break;
  case DS:
    setFixedSize(ds_width, ds_height);
    bm = QBitmap (ds_width, ds_height, ds_bits, TRUE);
    break;
  case SN:
    setFixedSize(sn_width, sn_height);
    bm = QBitmap (sn_width, sn_height, sn_bits, TRUE);
    break;
  case IOP:
    setFixedSize(iop_width, iop_height);
    bm = QBitmap (iop_width, iop_height, iop_bits, TRUE);
    break;
  case SPU:
    setFixedSize(spu_width, spu_height);
    bm = QBitmap (spu_width, spu_height, spu_bits, TRUE);
    break;
  case CPU:
    setFixedSize(cpu_width, cpu_height);
    bm = QBitmap (cpu_width, cpu_height, cpu_bits, TRUE);
    break;
  }
  setMask(bm);
  
}

/**
 * Set the type of this node at random with the preceding node as argument.
 * Returns true if this is an end of line
 */
bool Node::setRType(int preceding) {

  // this will indicate that we created an end of line Node
  bool stopHere = false;
  // the dice roll
  int dice = random() % 6;
  switch (preceding) {
  case CPU:
    switch (dice) {
    case 0:
    case 1:
    case 2:
      setType(SPU);
      break;
    case 3:
    case 4:
      setType(DS);
      break;
    case 5:
      // eol node
      stopHere = true;
      switch (random() % 3) {
      case 0:
        setType(SAN);
        break;
      case 1:
        setType(IOP);
        break;
      case 2:
        setType(SN);
        break;
      }
      break;
    }
  case SPU:
    switch (dice) {
    case 0:
      setType(CPU);
      break;
    case 1:
      setType(SPU);
      break;
    case 2:
    case 3:
      setType(DS);
      break;
    case 4:
    case 5:
      //eol node
      stopHere = true;
      switch (random() % 3) {
      case 0:
        setType(SAN);
        break;
      case 1:
        setType(IOP);
        break;
      case 2:
        setType(SN);
        break;
      }
    }
    break;
  case DS:
    switch (dice) {
    case 0:
    case 1:
      setType(CPU);
      break;
    case 2:
    case 3:
    case 4:
      setType(SPU);
      break;
    case 5:
      setType(DS);
      break;
    }
    break;
  default:
    setType(SPU);
    break;
  }
  return stopHere;
}

/**
 * Set the rating of the node to r
 */
void Node::setRating(int r) {
   rating = r;
   emit chgRating(r);
}

/**
 * Set a rating at random
 */
void Node::setRRating() {
  setRating(random() % 6 + 1);
}

/**
 * Set the description of the node
 */
void Node::setDesc(const QString & d) {
  description = d;
}

void Node::setNumber(int n) {number = n;}

/**
 * Add a given IC
 */
void Node::addIC(IC &ic) {

  ices->append(&ic);
  
  emit addedIC(ic);
  emit chgIC();

}

/**
 * Add necessary random ICs
 */
void Node::addIC() {

  IC *ic = new IC(getColor());

  // rating is -1 if there's a hidden IC
  if (ic->getRating() == TRAPPED) {
    // set a correct rating for the node
    ic->setRating();

    // make a new one
    IC *ic2 = new IC(getColor());
    // force it to be GRAY
    while (ic2->getRating() == -1 || ic2->getType() < GRAY)
      ic2->setRType(GRAY);
    ices->append(ic2);
  }
  ices->append(ic);

}

/**
 * Remove an IC by it's index
 */
void Node::removeIC(int index) {
  ices->remove(index);
  emit removedIC(index);
  emit chgIC();
}

/**
 * Add a given data file
 */
void Node::addData(Data &f) {
  datalist->append(&f);
  emit addedData(f);
  emit chgData();
}

/**
 * Add random data for this node
 */
void Node::addData() {

  // add 1 to 6 files
  int numFiles = random() % 6 + 1;

  for (int i = 0; i < numFiles; i++)
    datalist->append(new Data(getColor()));
}

/**
 * Remove a given Data file
 */
void Node::removeData(Data &f) {
  datalist->remove(&f);
  emit removedData(f);
  emit chgData();
}

/**
 * Remove a Data file given by its index
 */
void Node::removeData(int index) {
  datalist->remove(index);
  emit removedData(index);
  emit chgData();
}

/**
 * Returns the real QColor object for a given #def
 */
QColor Node::getRealColor(int c) {

  switch (c) {
  case BLUE:
    return QColor(blue);
    break;
  case GREEN:
    return QColor(green);
    break;
  case ORANGE:
    return QColor(yellow);
    break;
  case RED:
    return QColor(red);
    break;
  default:
    return QColor(green);
    break;
  }

}

/**
 * Trace the node on the QPainter paint
 */
void Node::traceNode(QPainter *paint, int c, int t) {

//   // get the real color
//   QColor rc = getRealColor(c);
//   paint->setPen( rc );
//   paint->setBrush( rc );
//   QPointArray * pa = 0;
//   QPointArray * pb = 0;
//   QRect *qr = 0;

//   // Vetical distance from the upper left of the widget to the 2 extreme points
//   int sigma = (int) SQRT3*genSize/4;
//   switch (t) {
//   case CPU:
//     pa = new QPointArray(7);
//     pa->putPoints(1,7, 2,sigma,
//                   genSize/4, 2,
//                   3*genSize/4, 2,
//                   genSize-2, sigma,
//                   3*genSize/4, genSize-2,
//                   genSize/4, genSize-2,
//                   2,sigma);
//     paint->drawPolygon(*pa);
//   case SPU:
//     pb = new QPointArray(7);
//     pb->putPoints(1,7, 0,sigma,
//                   genSize/4, 0,
//                   3*genSize/4, 0,
//                   genSize, sigma,
//                   3*genSize/4, genSize,
//                   genSize/4, genSize,
//                   0,sigma);
//     paint->drawPolygon(*pb);
//     break;
//   case IOP:
//     pa = new QPointArray(3);
//     pa->putPoints(1, 3, 0, genSize,
//                   0 + genSize/2, 0,
//                   0 + genSize, genSize);
//     paint->drawPolygon(*pa);
//     break;
//   case SN:
//     qr = new QRect(0,0,genSize,genSize);
//     paint->drawEllipse(*qr);
//     break;
//   case SAN:
//     qr = new QRect(0, 0, genSize, genSize/2);
//     pa = new QPointArray(*qr,FALSE);
//     paint->drawPolygon(*pa);
//     break;
//   case DS:
//     qr = new QRect(0,0,genSize,genSize);
//     pa = new QPointArray(*qr,FALSE);
//     paint->drawPolygon(*pa);
//     break;
//   }
}

void Node::ping(const QPoint &p) {

  // maybe this will be mask()?
  if (rect().contains(mapFromGlobal(p)))
    emit pingBack(this);
  
}

void Node::randomize(int t, int c) {

  // set random type with SAN as preceding node
  setRType(t);
  // set random color
  setRColor(c);
  // .. and rating
  setRRating();

  // add random datafiles
  addData();
  // and random ics
  addIC();

}

/******************************************************************************
 * Events
 */
void Node::mousePressEvent(QMouseEvent *moe) {

  // initial will be used to see of how much we moved
  initial->setX(moe->globalX());
  initial->setY(moe->globalY());
  dragging = false;

}

void Node::mouseReleaseEvent(QMouseEvent *moe) {

  // if not relecting another node
  if ( !(moe->state() & ShiftButton) )
    if (!dragging) // keep the selection intact if dragging
      emit deselect();
  // toglle the select
  if (getSelected()) {
    if (!dragging) unselect();
  } else select();
  dragging = false;

}

void Node::mouseMoveEvent (QMouseEvent *) {

  dragging = true;
  QPoint diff = QCursor::pos()-*initial;
  initial->setX(QCursor::pos().x());
  initial->setY(QCursor::pos().y());

  if (getSelected()) // if we're part of the selection, ask to be moved
    emit moved(diff);
  else
    dmove(diff); // if not, move ourselves

}

void Node::mouseDoubleClickEvent (QMouseEvent *) {

  new EditNode(this);
  
}

void Node::keyPressEvent (QKeyEvent *kpe) {

  switch (kpe->key()) {
  case Key_Backspace:
  case Key_Delete:
    emit deleting(this);
    close(TRUE);
    break;
  case Key_Return:
  case Key_Enter:
  case Key_Insert:
  case Key_Home:
  case Key_End:
  case Key_PageUp:
  case Key_PageDown:
    break;
  case Key_Left:
    // these move the node in key's direction
    emit moved(QPoint(-1,0));
    break;
  case Key_Up:
    emit moved(QPoint(0,-1));
    break;
  case Key_Right:
    emit moved(QPoint(1,0));
    break;
  case Key_Down:
    emit moved(QPoint(0,1));
    break;
  }
}

void Node::paintEvent (QPaintEvent *) {

  setBackgroundColor( *realColor );

  if (getSelected()) {
    QBitmap bm;
    switch (type) {
    case SAN:
      bm = QBitmap (san_width, san_height, sanf_bits, TRUE);
      break;
    case DS:
      bm = QBitmap (ds_width, ds_height, dsf_bits, TRUE);
      break;
    case SN:
      bm = QBitmap (sn_width, sn_height, snf_bits, TRUE);
      break;
    case IOP:
      bm = QBitmap (iop_width, iop_height, iopf_bits, TRUE);
      break;
    case SPU:
      bm = QBitmap (spu_width, spu_height, spuf_bits, TRUE);
      break;
    case CPU:
      bm = QBitmap (cpu_width, cpu_height, cpuf_bits, TRUE);
      break;
    }
    bitBlt (this, 0, 0, &bm, 0, 0, bm.width(), bm.height());
  }
  
}

Q_EXPORT QTextStream &operator<<( QTextStream &s, const Node &n ) {

  s << endl << "Node " << n.getNumber() << ":" << endl
    << "======";
//   for (const char *c = n.name(); c != 0; c++)
//     s << "=";
  s << endl << endl
    << "Type: ";
  switch (n.getType()) {
  case CPU:
    s << "CPU (Central Processing Unit)";
    break;
  case SPU:
    s << "SPU (Sub Processing Unit)";
    break;
  case SAN:
    s << "SAN (System Access Node)";
    break;
  case IOP:
    s << "I/OP (Input / Output Port)";
    break;
  case DS:
    s << "DS (Datastore)";
    break;
  case SN:
    s << "SN (Slave Node)";
    break;
  }
  s << endl;

  s << "Security code/rating: ";
  switch (n.getColor()) {
  case BLUE:
    s << "Blue";
    break;
  case GREEN:
    s << "Green";
    break;
  case ORANGE:
    s << "Orange";
    break;
  case RED:
    s << "Red";
    break;
  default:
    s << "???";
    break;
  }
  s << " - " << n.getRating() << endl
    << "Description: " << endl
    << n.getDesc() << endl << endl;

  QListIterator<IC> ices(n.getICsList());
  ices.toFirst();

  s << "ICs (" << ices.count() << "):" << endl
    << "===========" << endl << endl;

  for (; ices.current(); ++ices)
    s <<  (IC &) *ices.current() << endl;

  QListIterator<Data> datafiles(n.getDataList());
  datafiles.toFirst();
  s << endl << "Datafiles (" << datafiles.count() << "):" << endl
    << "================" << endl << endl;

  for (; datafiles.current(); ++datafiles)
    s << (Data &) *datafiles.current();

  return s;

  
}

Q_EXPORT QDataStream &operator<<( QDataStream &s, const Node &n ) {

  // put color, type, rating and position
  s << n.getColor() << n.getType() << n.getRating()
    << n.x() << n.y();
  if (n.isVisible())
    s << (INT8) 1;
  else s << (INT8) 0;

  QListIterator<IC> ices(n.getICsList());
  ices.toFirst();

  // have a way to know how many IC structures to get
  s << ices.count();

  for (; ices.current(); ++ices)
    s <<  (IC &) *ices.current();

  QListIterator<Data> datafiles(n.getDataList());
  datafiles.toFirst();
  s << datafiles.count();

  for (; datafiles.current(); ++datafiles)
    s << (Data &) *datafiles.current();
  return s;
}

Q_EXPORT QDataStream &operator>>( QDataStream &s, Node &n ) {

  // get and set fields
  int color, type, rating, x, y;
  INT8 visible;
  s >> color >> type >> rating >> x >> y >> visible;

  n.setColor(color);
  n.setType(type);
  n.setRating(rating);
  n.move(QPoint(x,y));
  if (visible == 1) n.show();
  else n.hide();
  
  int count;
  s >> count;
  IC *currentIC;
  for (int i = 0; i < count; i++) {
    currentIC = new IC();
    s >> *currentIC;
    n.addIC(*currentIC);
  }

  s >> count;
  Data *currentD = 0;
  for (int i = 0; i < count; i++) {
    currentD = new Data();
    s >> *currentD;
    n.addData(*currentD);
  }
  return s;
  
}
