/****************************-*-c++-*-*********************************
 * $Id: Matrix.cpp,v 0.13 1999/08/26 23:10:29 spidey Exp spidey $
 **********************************************************************
 * Matrix Widget that is the Matrix itself...
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Matrix.cpp,v $
 * Revision 0.13  1999/08/26 23:10:29  spidey
 * Removed nextNode properly
 * (addNode(), Randomize, ~Node, Node(.*) updated properly)
 *
 * Revision 0.12  1999/08/24 19:45:48  spidey
 * + Improved randomize() code (dialog window, progress bars, position, etc...)
 * + Written getPixmap() routine
 * + Cleared >> and << code
 *
 * Revision 0.11  1999/07/23 12:06:24  spidey
 * Added randomize() code
 * Checks for single CPU
 * Listens to modified() signals from nodes
 *
 * Revision 0.10  1999/07/22 13:35:13  spidey
 * Added - modified()
 *       - node numbering
 *       - QTextStream writing
 *
 * Revision 0.9  1999/07/20 01:35:51  spidey
 * Solved core dumps with save: QList of the nodes in the Matrix
 *
 * Revision 0.8  1999/06/07 15:51:23  spidey
 * Implemented serialisation of nodes and vertices
 * 	Still problems with save & quit, which causes a core dump.
 * + >>() <<()
 * + addNode(Node*);
 * + addVertex(QPoint, QPoint);
 *
 * Revision 0.7  1999/06/01 17:54:37  spidey
 * Comments and single clic adding
 *
 * Revision 0.6  1999/06/01 01:42:14  spidey
 * Added necessary slots/signals to process node deletion
 * deleting a node now signals vertices to delete if they're connected
 * eventFilter discards null pointers
 *
 * Revision 0.5  1999/06/01 00:32:22  spidey
 * Changed radically the vertex adding technique. For the best, I think.
 * + Now only two connection for vertices:
 *   - one for the ping, and one for the pingBack/addNodeToVertex()
 *   - removed the createVFrom/To()
 * + AddNodeToVertex(Node) makes the node the first or second part of a
 *   vertex
 * + The event filter filters every mouseEvent with the Control key.
 *
 * Revision 0.4  1999/05/31 14:45:06  spidey
 * Implemented vertex drawing (not selection yet)
 * Most functions now use & instead of * as parameters and return values
 * Added a few slots and signals to handle vertex creation
 *
 * Revision 0.3  1999/05/28 17:55:47  spidey
 * + Cleaned the code
 * + implemented multiple node selection:
 *         + add/removeSelected(Node*), unselect() slots
 *         + necessary connections made in addNode()
 *
 * Revision 0.2  1999/05/26 18:40:35  spidey
 * Addition of nodes implemented!!!
 *
 *********************************************************************/

#include "Matrix.h"
#include <qcolor.h>
#include <qcursor.h>
#include <qobjcoll.h>
#include <stdlib.h>
#include <qprogressdialog.h>
#include <qdialog.h>
#include <qlayout.h>
#include <qspinbox.h>
#include <qpushbutton.h>
#include <qpixmap.h>

#ifdef DEBUG
#include <iostream.h>
#endif

/******************************************************************************
 * Constructors
 */
Matrix::Matrix( QWidget *parent )
  : QWidget (parent, "matrix widget") {

  modify();
  globalColor = BLUE, color = GREEN, type = SPU, rating = 3;
  nodeList = new QList<Node>();
  // instantiate the objects
  selected = new QList<Node>();
  vertices = new QList<Vertex>();
  vertices->setAutoDelete(true);
  fromNode = 0;
  setBackgroundColor( white );
  
}

/**
 * Destructor
 */
Matrix::~Matrix() {

  delete nodeList;
  delete selected;
  delete vertices;
  
}

/**
 * Return a pixmap image of this Matrix
 */
QPixmap Matrix::getPixmap() {

  QPixmap output(size());
  bitBlt (&output, 0, 0 , this, 0, 0, -1, -1, CopyROP);
  for (int i = 0; i < nodeCount(); i++) {
    
    bitBlt (&output, nodeAt(i)->x(), nodeAt(i)->y(), nodeAt(i), 0, 0, -1, -1, CopyROP);

  }
  return output;

}

/******************************************************************************
 * Slots
 */
void Matrix::randomize() {

  // a dialog and its layout to ask the user about some parameters
  QDialog *configRandom = new QDialog(0, "Random Config", TRUE);
  QGridLayout *lay = new QGridLayout(configRandom, 3, 2, 3);

  // ok/cancel buttons
  QPushButton *ok, *cancel;
  ok = new QPushButton( "Proceed", configRandom );
  cancel = new QPushButton( "Cancel", configRandom );
  
  QSpinBox *maxNodes = new QSpinBox(1, 1025, 1, configRandom, "max Nodes");
  QLabel *nodes_l = new QLabel
    (maxNodes, "&Maximum number of nodes to create in one branch: ", configRandom);    
  QSpinBox *spreadProb = new QSpinBox(0, 100, 1, configRandom, "spread prob");
  QLabel *spread_l = new QLabel
    (spreadProb, "&Probability of creating a new branch: ", configRandom);

  maxNodes->setValue(10);
  spreadProb->setValue(10);
  
  configRandom->setCaption("Create random nodes...");
  
  // setup item sizes
  cancel->setMinimumSize(cancel->sizeHint());
  cancel->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  ok->setMinimumSize(ok->sizeHint());
  ok->setMaximumSize(QSize(1000,cancel->minimumSize().height() ));
  maxNodes->setFixedSize(maxNodes->sizeHint());
  spreadProb->setFixedSize(spreadProb->sizeHint());

  // throw them in a layout
  lay->addWidget(nodes_l, 0, 0);
  lay->addWidget(maxNodes, 0, 1);
  lay->addWidget(spread_l, 1, 0);
  lay->addWidget(spreadProb, 1, 1);
  lay->addWidget(ok, 2, 0 );
  lay->addWidget(cancel, 2, 1);
  lay->activate();

  // few connections to add or not
  connect( ok, SIGNAL(clicked()),
	   configRandom, SLOT(accept()) );
  connect( cancel, SIGNAL(clicked()),
	   configRandom, SLOT(reject()) );

  // this is a modal dialog, send it the flow of control
  if (configRandom->exec()) {
    // ok was pressed
    Node *curNode, *nextNode;
    int x = 30, y = 30, dx = 30, dy = 30;

    if ( nodeList->count() == 0 ) {
      curNode = new Node(this, 1, 1, 1);
      curNode->randomize(SAN, globalColor);
      curNode->setType(SAN);
      addNode(curNode);
      curNode->move(QPoint(x, y));
    } else {
      if (selected->isEmpty())
	curNode = nodeList->getLast();
      else
	curNode = selected->getLast();
      x = curNode->x();
      y = curNode->y();
    }

    nextNode = new Node(this, 1, 1, 1);
    nextNode->randomize(curNode->getType(), globalColor);

    addNode(nextNode);
    x += dx;
    nextNode->move(QPoint(x,y));
    addVertex(curNode, nextNode);

    curNode = nextNode;

    randomize(curNode, maxNodes->value(), spreadProb->value());

  }
    
}

void Matrix::randomize(Node *curNode, int maxNodes = 10, int spreadProb = 10) {
  
  Node *nextNode;
  int x = curNode->x(), y = curNode->y(), dx = 30, dy = 30;
  
  // if the last node installed was a 'auxiliary' (IOP, SN or SAN)
  bool lastNodeWasAux = FALSE;
  // the distance from the node if adding an auxiliary
  int ddy = dy;

  
  QProgressDialog *prog = new QProgressDialog
    ( "Creating nodes in a new branch...", "Abort branch", maxNodes, 0, "progress dialog" );
  prog->setMinimumDuration(500);
  prog->show();
  
  for (int i = 0; i < maxNodes || !hasCPU; i++) {
    if (prog->wasCancelled())
      break;
    prog->setProgress(i+1);
    nextNode = new Node(this, 1, 1, 1);
    nextNode->randomize(curNode->getType(), globalColor);
    addNode(nextNode);
    switch (nextNode->getType()) {
    case SN:
    case IOP:
    case SAN:
      if (lastNodeWasAux)
	ddy = -ddy;
      else
	ddy = dy;
      nextNode->move(QPoint(x,y+ddy));
      addVertex(curNode, nextNode);
      lastNodeWasAux = TRUE;
      break;
    default:
      short change = random()%100 < spreadProb;
      if (change) {
	ddy = -ddy;
	nextNode->move(QPoint(x, y+ddy));
      } else {
	x+=dx;
	nextNode->move(QPoint(x, y));
      }
      addVertex(curNode, nextNode);
      if (change)
	randomize(nextNode);
      else
	curNode = nextNode;
      lastNodeWasAux = FALSE;
      break;
    }
  }
  prog->setProgress(prog->totalSteps());
  prog->reset();
}

void Matrix::addSelected(Node *n) {
  // if the node moves, we move all selected nodes
  connect(n, SIGNAL(moved(const QPoint &)),
          this, SIGNAL(moved(const QPoint &)));
  connect(this, SIGNAL(moved(const QPoint &)),
          n, SLOT(dmove(const QPoint &)));
  selected->append(n);
}

void Matrix::removeSelected(Node *n) {
  // remove the connections that were made in addSelected
  disconnect(n, SIGNAL(moved(const QPoint&)),
             this, SIGNAL(moved(const QPoint&)));
  disconnect(this, SIGNAL(moved(const QPoint&)),
             n, SLOT(dmove(const QPoint&)));
  selected->remove(n);
}

void Matrix::unselect() {

  // unselect them all
  selected->first();
  while(!selected->isEmpty())
    selected->current()->unselect();

}

bool Matrix::addNode(const QPoint &p) {

  // here we create the new node
  Node *n = new Node(this, type, color, rating);
  // ??? could not allocate memory?
  if ( n == 0 ) return false;
  // put it at the given point
  n->move(p);

  addNode(n);
  return true;

}

void Matrix::addNode(Node *n) {

  if (n->getType() == CPU)
    if (hasCPU) n->setType(SPU);
    else hasCPU = true;
  // connections...
  // the node tells the matrix it's selected so it can add it to the list
  connect (n, SIGNAL(selected(Node*)),
           this, SLOT(addSelected(Node*)));
  // idem for unselection
  connect (n, SIGNAL(unselected(Node*)),
           this, SLOT(removeSelected(Node*)));
  // if it's a single click on the node, unselect()
  connect (n, SIGNAL(deselect()),
           this, SLOT(unselect()));
  // this is the mechanism to find if there's a node at a QPoint...
  connect (this, SIGNAL(ping(const QPoint&)),
           n, SLOT(ping(const QPoint&)));
  // ...and this is mechanism for the node to signal it's there and add itself
  // to the current creating vertex
  connect (n, SIGNAL(pingBack(Node *)),
           this, SLOT(addNodeToVertex(Node *)));
  // if a node is deleting, give us a chance to make some cleanup
  connect (n, SIGNAL(deleting(Node *)),
           this, SLOT(removeNode(Node *)));
  // if a node is modified, the entire matrix is modified
  connect (n, SIGNAL(modified()),
	   this, SIGNAL(modified()));
  // this class will handle some mouseevents for the nodes
  n->installEventFilter(this);
  // if we got here, the memory was allocated right

  n->setNumber(nodeList->count());
  nodeList->append(n);
  modify();
  
}

void Matrix::removeNode(Node *n) {

  if (n->getType() == CPU)
    hasCPU = false;
  nodeList->remove(n);
  for (unsigned int i = 0; i < nodeList->count(); i++)
    nodeList->at(i)->setNumber(i);
  removeSelected(n); // remove it from any selection
  disconnect(n);  // remove all the connections to this node
  
}

void Matrix::addNodeToVertex(Node *n) {
 
  // if it is called for the first time, we keep in mind the node to add it later
  if (fromNode == 0) fromNode = n;
  else {
    // we add this node and the node we kept to a vertex
    addVertex(fromNode, n);
    // start anew
    fromNode = 0;
  }
  
}

void Matrix::addVertex(Node *from, Node *to) {

  // check if we don't add a vertex to itself and for null pointers
  if (from != to && from != 0 && to != 0) {
    // allocate mem for the vertex
    Vertex *v = new Vertex(from, to);
    if (v == 0) {
#ifdef DEBUG
      cerr << "Can't allocate memory for vertex" << endl;
#endif
      return;
    }
    // we add it to the list
    vertices->append(v);
    // if one of the nodes is deleted, die.
    connect(from, SIGNAL(deleting(Node*)),
            v, SLOT(del()));
    connect(to, SIGNAL(deleting(Node*)),
            v, SLOT(del()));
    // if the vertex wants to die, make it die here.
    connect(v, SIGNAL(deleting(Vertex*)),
            this, SLOT(removeVertex(Vertex*)));
    // or else the vertex won't be seen at first...
    update();
    modify();
  } else 
#ifdef DEBUG
    cerr << "Can't add a vertex to himself/null vertex pointer!";
#endif
    ;
}

void Matrix::addVertex(const QPoint &p1, const QPoint &p2) {

  emit ping(mapToGlobal(p1));
  emit ping(mapToGlobal(p2));
  // if we hit one or nothing, make as nothing happened
  fromNode = 0;
  
}

void Matrix::removeVertex(Vertex *v) {

  // when we remove it from the list, it is also deleted
  vertices->remove(v);
  // see the effects immediatly
  update();
  
}

// void Matrix::clear() {

//   for (unsigned int i = 0; i < nodeList->count(); i++) {
//     disconnect( nodeList->at(i) );
//     removeSelected( nodeList->at(i) );
//     nodeList->at(i)->hide();
//     removeChild ( nodeList->at(i) );
//   }
//   vertices->clear();
//   nodeList->clear();

//   delete nodeList;
//   delete selected;
//   delete vertices;

//   // we must remove all childs of the Matrix!!!
  
//   nodeList = new QList<Node>();
//   selected = new QList<Node>();
//   vertices = new QList<Vertex>();
//   vertices->setAutoDelete(true);
  
//   fromNode = 0;
//   delete nextNode;
//   nextNode = new Node(this, SPU, GREEN, 3);
//   nextNode->hide();
  
//   hasCPU = false;
// }

/******************************************************************************
 * Events
 */
bool Matrix::eventFilter( QObject *object, QEvent *event) {

  // check for null pointers
  if (object != 0 && object->isA("Node"))
    switch (event->type()) {
    case QEvent::MouseButtonPress:
      // filter the event only if Ctrl is down
      if ( ( (QMouseEvent *) event)->state() & ControlButton) {
        // add the node as the first part of a vertex
        addNodeToVertex((Node*)object);
        return TRUE; // event handled, don't send it to the node
      }
      break;
    case QEvent::MouseMove:
      // here, we should draw the vertex when it's created
      if ( ( (QMouseEvent *) event)->state() & ControlButton)
        return TRUE; // don't pass this to the node anyways...
    case QEvent::MouseButtonRelease:
      if ( ( (QMouseEvent *) event)->state() & ControlButton) {
        if (fromNode != 0) // only if we had a fromNode at first
          // we can't use *object, as it is still the starting point!
          emit ping(((QMouseEvent*) event)->globalPos()); // find the node
        fromNode = 0; // in any case, we must not have a pending vertex creating
        return TRUE;
      }
      break;
    default:
      break;
    }
  return FALSE; // dispatch the event normally
  
}

void Matrix::mouseDoubleClickEvent (QMouseEvent *) {


}

void Matrix::mouseReleaseEvent (QMouseEvent* moe) {

  // add a node on a single click if there's no selection
  if (selected->isEmpty())
    if (addNode(moe->pos())) ; // y�
    else // warn for null pointers
      warning("Not enough memory to add a new node."); // oups. not enough mem.
  // unselect() anyways
  unselect();
  
}

void Matrix::paintEvent( QPaintEvent * ) {

  // scan the vertices to draw them
  QListIterator<Vertex> vertIt(*vertices);
  vertIt.toFirst();

  QPainter paint(this);

  // the vertices are black
  paint.setPen(black);

  // clean all the area first
  paint.eraseRect(rect());

  for (; vertIt.current(); ++vertIt) {
    // we get the center points of the 2 nodes
    QPoint a = vertIt.current()->getPointA();
    QPoint b = vertIt.current()->getPointB();
    QPointArray p(2);
    // put them in an array
    p.setPoint(0,a);
    p.setPoint(1,b);
    
    // we draw the vertex
    paint.drawPolyline(p);

  }
    
}

Q_EXPORT QTextStream &operator<<( QTextStream &s, const Matrix &m ) {

  s << endl << endl;
  int *colors = new int[4];
  int *types = new int[6];
  for (int i = 0; i < 4; i++) colors[i] = 0;
  for (int i = 0; i < 6; i++) types[i] = 0;
  
  int dataCount = 0,
    icCount = 0;

  Node *node = 0;

  for (int i = 0; i < m.nodeCount(); i++) {
    int c, t;

    node = m.nodeAt(i);

    c = node->getColor();
    if (c > 0 && c < 4)
      colors[c]++;
    t = node->getType();
    if (t > 0 && t < 6)
      types[t]++;
    dataCount += node->getDataList().count();
    icCount += node->getICsList().count();

  }

  s << "Node Count: " << m.nodeCount() << endl
    << "===========" << endl
    << "Type:"
    << "CPUs: " << types[0] << endl
    << "SPUs: " << types[1] << endl
    << "SANs: " << types[2] << endl
    << "DSs: " << types[3] << endl
    << "SNs: " << types[4] << endl
    << "IOPs: " << types[5] << endl << endl
    << "Security Codes: " << endl
    << "===============" << endl
    << "Blue: " << colors[0] << endl
    << "Green: " << colors[1] << endl
    << "Orange: " << colors[2] << endl
    << "Red: " << colors[3] << endl << endl
    << "IC and Datafiles count: " << endl
    << "=======================" << endl
    << "Ices: " << icCount << endl
    << "Datafiles: " << dataCount << endl << endl
    << "Complete node description and listing:" << endl
    << "======================================" << endl;
  
  for (int i = 0; i < m.nodeCount(); i++) {
    node = m.nodeAt(i);
    s << *node;
  }

  QList<Vertex> *vertices = m.getVertices();

  s << endl << "Vertices (" << vertices->count() << "):"
    << endl << "=========" << endl << endl;
  
  for (unsigned int i = 0; i < vertices->count(); i++) {
    s << vertices->at(i)->getNumA() << " - "
      << vertices->at(i)->getNumB() << "\t";
    if (i % 4 == 3) s << endl;
  }

  s << endl << "All done." << endl;

  return s;
}

Q_EXPORT QDataStream &operator<<( QDataStream &s, const Matrix &m ) {

  // this constructs the list of Nodes
//   const QObjectList *ol = m.children();
//   QObjectListIt it(*ol);
//   cerr << "QObjectList *ol: " << ol << endl;
//   cerr << "QObjectListIt it: " << *it << endl;
  // let's write how many we have
  s << m.nodeCount();

  Node *node = 0;
  //while ( (it.current()) != 0) { // for each object found
  for (int i = 0; i < m.nodeCount(); i ++) {
//     if (it.current()->isA("Node")) {
    //node = (Node*) it.current();
    node = m.nodeAt(i);
    //      if (node->isVisible())
    s << *node; // push it in the stream
    //    }
    //++it; // next
  }
  //delete ol;

  const QList<Vertex> vlist = *m.getVertices();
  QListIterator<Vertex> vit(vlist);
  s << vit.count();
  vit.toFirst();
  while ( vit.current() != 0 ) {
    QPoint a = vit.current()->getPointA();
    QPoint b = vit.current()->getPointB();
    s << a.x() << a.y() << b.x() << b.y();
    ++vit;
  }
  return s;
}  

/**********
 * This is not working correctly if there's something in the matrix...
 */
Q_EXPORT QDataStream &operator>>( QDataStream &s, Matrix &m ) {

  int count = 0;
  s >> count;

  Node *curNode = 0;
  for (int i = 0; i < count; i++) {
    curNode = new Node(&m);
    s >> *curNode;
    m.addNode(curNode);
  }

  count = 0;
  s >> count;

  int x1, y1, x2, y2;
  for (int i = 0; i < count; i++) {
    s >> x1 >> y1 >> x2 >> y2;
    m.addVertex(QPoint(x1,y1), QPoint(x2,y2));
  }

  m.update();  

  return s;
}
