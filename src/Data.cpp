/****************************-*-c++-*-*********************************
 * $Id: Data.cpp,v 1.5 1999/09/27 17:07:32 spidey Exp spidey $
 **********************************************************************
 * File list
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Data.cpp,v $
 * Revision 1.5  1999/09/27 17:07:32  spidey
 * Simply moved the stream functions to the end of the file
 * and added Data()
 *
 * Revision 1.4  1999/07/22 13:27:44  spidey
 * QTextStream writing
 *
 * Revision 1.3  1999/06/01 19:41:50  spidey
 * Serialisation
 *
 * Revision 1.2  1999/06/01 17:22:16  spidey
 * comments
 *
 * Revision 1.2  1999/06/01 17:17:29  spidey
 * Comments
 *
 * Revision 1.1  1999/05/28 17:57:33  spidey
 * Initial revision
 *
 *********************************************************************/

#include "Data.h"
#include "Node.h"
#include <stdlib.h>

/**
 * Empty constructor
 */
Data::Data() {

  filename = desc = "";

}

/**
 * Default constructor.
 * Will create size and value of data randomly by the color of the node
 * filename and desc are left empty (=0)
 */
Data::Data(INT16 color) {

  setSize();
  setRValue(color);
  filename = desc = "";

}

/**
 * Complete constructor
 * Sets filename, value, size and description of data.
 */
Data::Data(QString name, INT16 s, INT16 v, QString d) {
  filename = name;
  size = s;
  value = v;
  desc = d;
}

/**
 * Destructor
 */
Data::~Data() {}

/**
 * Sets the filename
 */
void Data::setFilename(QString f) {

  filename = f;
  emit chgFilename(filename);
  
}

/**
 * Set a random size as per rules on p. 193
 */
void Data::setSize() {setSize((random() % 10 + 2) *10);}

/**
 * Sets the size to s
 */
void Data::setSize(INT16 s) {

  size = s;
  emit chgSize(size);

}

/**
 * Static function to return a random value from a given node color
 */
INT16 Data::getRValue(INT16 color) {

  if (color == BLUE) return 0;
  switch (random()%10) {
  case 0:
  case 10:
    return 0;
  case 1:
  case 2:
    switch (color) {
    case GREEN:
      return 50;
    case ORANGE:
      return 100;
    case RED:
      return 350;
    }
  case 3:
  case 4:
  case 5:
    switch (color) {
    case GREEN:
      return 100;
    case ORANGE:
      return 250;
    case RED:
      return 500;
    }
  case 6:
  case 7:
  case 8:
    switch (color) {
    case GREEN:
      return 500;
    case ORANGE:
      return 1000;
    case RED:
      return 5000;
    }
  case 9:
    switch (color) {
    case GREEN:
      return 1000;
    case ORANGE:
      return 5000;
    case RED:
      return 10000;
    }
  default:
    return 0;
  }
}

/**
 * Sets the value of this data random.
 */
void Data::setRValue(INT16 color) {

  setValue(size*getRValue(color));
}

/**
 * Sets value to v
 */
void Data::setValue(INT16 v) {

  value = v;
  emit chgValue(value);

}

/**
 * Sets description to d
 */
void Data::setDesc(QString d) {

  desc = d;
  emit chgDesc(desc);
  
}

/**
 * Get a text description of this datafile.
 */
QString Data::getText() const {

//   sprintf(txt, "%s: %.5c Mp, %.5c Y, %s", filename, size, value, desc);

  return "";
}

QString Data::getShortText() const {

  return filename + tr(": %1 Mp, %2 Nuyen (").arg(size).arg(value) + desc;  
}

/**
 * Data stream method
 */
QTextStream &operator<< ( QTextStream &s, const Data &d) {

  return s << '"' << d.getFilename() << "\": " << d.getSize() << " Mp, "
	   << d.getValue() << " Nuyen (" << d.getDesc() << ")";

}

QDataStream &operator<< ( QDataStream &s, const Data &d) {

  return s << d.getFilename() << (INT16) d.getSize()
           << (INT16) d.getValue() << d.getDesc();

}

QDataStream &operator>> ( QDataStream &s, Data &d) {

  QString filename;
  QString desc;
  INT16 size, value;
  s >> filename >> size >> value >> desc;
  d.setFilename(filename);
  d.setSize(size);
  d.setValue(value);
  d.setDesc(desc);
  return s;
  
}
