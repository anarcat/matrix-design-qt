/****************************-*-c++-*-*********************************
 * $Id: Node.h,v 0.15 1999/08/26 22:46:59 spidey Exp spidey $
 **********************************************************************
 * Node definition
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Node.h,v $
 * Revision 0.15  1999/08/26 22:46:59  spidey
 * Null check-in to keep up with Node.cpp
 *
 * Revision 0.14  1999/08/26 20:13:10  spidey
 * Null check-in to keep up with Node.cpp
 *
 * Revision 0.13  1999/08/24 19:31:09  spidey
 * + genSize is now only a def
 * + added a int Color argument to randomize
 *
 * Revision 0.12  1999/07/22 14:26:32  spidey
 * Added: 	- node numbering
 *     	- modified() signal
 * 	- QTextStream writing
 *
 * Revision 0.11  1999/07/22 01:15:22  spidey
 * Added description field and necessary functions
 *
 * Revision 0.10  1999/07/22 00:26:31  spidey
 * Fixes Rating<->color bug.
 * moves random stuff to Randomize() function
 *
 * Revision 0.9  1999/06/01 20:29:32  spidey
 * Serialisation
 *
 * Revision 0.8  1999/06/01 19:06:41  spidey
 * Comments
 *
 * Revision 0.7  1999/06/01 01:44:24  spidey
 * Added the deleting(Node*) signal
 *
 * Revision 0.6  1999/06/01 00:39:22  spidey
 * Darn... this one too's out of sync...
 *
 * Revision 0.5  1999/06/01 00:38:28  spidey
 * Removed the mouseDown parameter altogether.
 * Modified mousePressed and mouseReleased events. Now the control's more up to Matrix.
 *
 * Revision 0.4  1999/05/28 17:53:01  spidey
 * - int dx, dy is now the QPoint* initial
 * + RealColor is now a pointer
 * + getSelected() returns true if selected
 * + (un)selecte(), dmove(QPoint*) slots
 * + random creation slots (e.g. setRType())
 * + deselect(), (un)selected(), moved() signals
 *
 * Revision 0.3  1999/05/27 04:37:11  spidey
 * Added Data and IC structures
 * Implemented appropriate methods
 *
 * Revision 0.2  1999/05/26 19:12:58  spidey
 * Added:
 * - #defs for node colors and types
 * - color, rating, and type fields
 * - many constructors
 *********************************************************************/

#ifndef NODE_H
#define NODE_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

/**
 * The nodes defines these types
 */
#define CPU 0
#define SPU 1
#define SAN 2
#define DS  3
#define SN  4
#define IOP 5

/**
 * and these colors
 */
#define BLUE 0
#define GREEN 1
#define ORANGE 2
#define RED 3

#include <qwidget.h>
#include <qpainter.h>
#include <qlist.h>
#include "IC.h"
#include "Data.h"

/**
 * Generic node size
 */
#define genSize 20

class Node : public QWidget {

  Q_OBJECT
  
private:

  // The node's color, type and rating
  int color, type, rating, number;

  // TRUE if the Node is selected
  bool selection;

  // This indicates that the node is currently being dragged
  bool dragging;
  
  // This is the color with which the node will be drawn.
  QColor *realColor;

  // The IC list
  QList<IC> *ices;

  // The datalist.
  QList<Data> *datalist;

  // Initial position when a mousePressed is sent
  QPoint *initial;

  // The description of the node
  QString description;

public:

  Node( QWidget *parent);

  /**
   * Creates a node with the given parent, type, color and rating
   */
  Node( QWidget *parent, int t, int c, int r);

  /**
   * Copy constructor
   */
  Node( Node * n);

  /**
   * This is probably unused and will be deleted
   */
  Node();
  
  /**
   * Destructor
   */
  ~Node();

  /**
   * Get various fields
   */
  int getType() const {return type;}
  int getRating() const {return rating;}
  int getColor() const {return color;}
  QColor& getRealColor() {return *realColor;}
  bool getSelected() {return selection;}
  const QPoint getCenter() {return QPoint(x()+width()/2, y()+height()/2);}
  const QString getDesc() const {return description;}
  int getNumber() const {return number;}
  
  const QList<Data>& getDataList() const {return *datalist;}
  const QList<IC>& getICsList() const {return *ices;}
  
  /**
   * This contains the corrects #def->qcolor bindings
   */
  static QColor getRealColor(int c);
    
  /**
   * Paints a given node on any QPainter
   */
  static void traceNode(QPainter *paint, int c, int t);

public slots:

  void select();
  void unselect();

  // this one moved by diffing last position
  void dmove(const QPoint&);
  
  void setNode(Node &n);

  void setRColor(int c);
  void setColor(int c);
  bool setRType(int t);
  void setType(int t);
  void setRRating();
  void setRating(int r);
  void setDesc(const QString& d);
  void setNumber(int n);
  
  void addIC();
  void addIC(IC &ic);
  void removeIC(int index);
  void addData();
  void addData(Data &f);
  void removeData(Data &f);
  void removeData(int i);

  // will emit pingBack if QPoint in rect()
  void ping(const QPoint &);

  /**
   * Randomize all stats of the Node
   */
  inline void randomize(int c) {randomize(SAN,c);}
  void randomize(int t, int c);

signals:

  // sent when we want to deselect other nodes
  void deselect();
  
  void selected(Node *);
  void unselected(Node *);

  void moved(const QPoint &);

  // sent before deletion
  void deleting(Node *);
  
  void chgType(int);
  void chgColor(int);
  void chgRating(int);

  void modified();
  
  void chgIC();
  void chgData();
  void addedIC(IC &);
  void removedIC(int);
  void addedData(Data &);
  void addedData();
  void removedData(Data &);
  void removedData(int);

  void pingBack(Node *);
  
protected:

  void paintEvent( QPaintEvent * );
  void mousePressEvent( QMouseEvent *);
  void mouseReleaseEvent( QMouseEvent *);
  void mouseMoveEvent( QMouseEvent *);
  void mouseDoubleClickEvent ( QMouseEvent * );
  void keyPressEvent (QKeyEvent *);
  
  
}; // class Node

Q_EXPORT QTextStream &operator<<( QTextStream &, const Node & );
Q_EXPORT QDataStream &operator<<( QDataStream &, const Node & );
Q_EXPORT QDataStream &operator>>( QDataStream &, Node & );

#endif
