 /****************************-*-c++-*-*********************************
 * $Id: Data.h,v 1.5 1999/09/27 17:06:04 spidey Exp spidey $
 **********************************************************************
 * File list
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: Data.h,v $
 * Revision 1.5  1999/09/27 17:06:04  spidey
 * Added the empty Data() constructor
 *
 * Revision 1.4  1999/07/22 13:26:42  spidey
 * QTextStream writing
 * inlined a few funcs
 * added get*Text()
 *
 * Revision 1.3  1999/06/01 19:41:40  spidey
 * Added serialisation
 *
 * Revision 1.2  1999/06/01 17:22:38  spidey
 * Comments
 *
 * Revision 1.1  1999/05/28 17:57:28  spidey
 * Initial revision
 *
 *********************************************************************/

#ifndef DATA_H
#define DATA_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include <qobject.h>
#include <qdatastream.h>
#include <qtextstream.h>

class Data : public QObject {

  // This sends some signals so...
  Q_OBJECT
  
 private:

  /**
   * These are self-explanatory
   */
  QString filename, desc;
  INT16 size;
  INT16 value;
  
 public:

  /**
   * Create the node with all empty fields
   */
  Data();
  /**
   * Make a random Datafile in a node of color 'color'
   */
  Data(INT16 color);
  /**
   * Create a specific datafile
   */
  Data(QString name,
       INT16 size,
       INT16 value,
       QString desc);

  /**
   * Destructor
   */
  ~Data();

  inline QString getFilename() const {return filename;}
  inline INT16 getSize() const {return size; }
  inline INT16 getValue() const {return value;}
  inline QString getDesc() const {return desc;}

  /**
   * This returns a random value based on color
   */
  static INT16 getRValue(INT16);
  
  QString getText() const;
  QString getShortText() const;
  
public slots:
  /**
   * These set the appropriate field
   */
  void setFilename(QString);
  void setSize();
  void setSize(INT16);
  void setRValue(INT16);
  void setValue(INT16);
  void setDesc(QString);

signals:
  /**
   * These are sent when the appropriate field changes
   */
  void chgFilename(QString);
  void chgSize(INT16);
  void chgValue(INT16);
  void chgDesc(QString);

};

/**
 * Data stream method
 */
Q_EXPORT QTextStream &operator<< ( QTextStream &, const Data &);
Q_EXPORT QDataStream &operator<< ( QDataStream &, const Data &);
Q_EXPORT QDataStream &operator>> ( QDataStream &, Data &);

#endif
