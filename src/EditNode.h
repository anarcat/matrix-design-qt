/****************************-*-c++-*-*********************************
 * $Id: EditNode.h,v 0.4 1999/07/23 02:28:28 spidey Exp spidey $
 **********************************************************************
 * QTabDialog Implementation to edit Node objects
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: EditNode.h,v $
 * Revision 0.4  1999/07/23 02:28:28  spidey
 * Added the IC editing capabitilies
 * Added a QListBox: icList
 *
 * Revision 0.3  1999/07/22 01:22:58  spidey
 * Added description editiion field
 *
 * Revision 0.2  1999/06/08 14:31:36  spidey
 * Just to keep in synch with the .cpp file.
 *
 * Revision 0.1  1999/05/28 17:58:59  spidey
 * *** empty log message ***
 *
 *********************************************************************/

#ifndef EDITNODE_H
#define EDITNODE_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif

#include "Node.h"
#include <qtabdialog.h>
#include <qmultilinedit.h>
#include <qlistbox.h>

class EditNode : QTabDialog {

  Q_OBJECT

    private:
  
  Node *editedNode;
  Node *original;
  QMultiLineEdit *description;
  QListBox *icList;
  QListBox *dataList;
  
    public:
  EditNode(Node *n);
  ~EditNode();

public slots:
  
  void apply();
  inline void addIC() {addIC(0);}
  void addIC(IC *);
  void delIC();
  void modIC();

  // addition of Data
  inline void addData() {addData(0);}
  // addData and modData procedure
  void addData(Data *);
  // deletion of Data
  void delData();
  // modification of Data. Calls addData
  void modData();

  void updateIClist();
  void updateDataList();
  //  void cancel();
  
};

#endif
