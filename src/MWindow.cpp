/****************************-*-c++-*-*********************************
 * $Id: MWindow.cpp,v 0.8 1999/08/26 23:37:06 spidey Exp spidey $
 **********************************************************************
 * The Main window
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: MWindow.cpp,v $
 * Revision 0.8  1999/08/26 23:37:06  spidey
 * Merged the 2 constructors into the initialize() function
 * "Save as Report..." -> "Save as text"
 * Added saveImage() that streams matrix->getPixmap() into a file
 *
 * Revision 0.7  1999/07/23 22:26:36  spidey
 * Added modified() connection to matrix and modify() procedures
 *
 * Revision 0.6  1999/07/22 13:23:04  spidey
 * Implemented Save As and save report
 *
 * Revision 0.5  1999/06/07 15:54:15  spidey
 * Implemented load() and save()
 *
 * Revision 0.4  1999/05/28 18:07:10  spidey
 * *** empty log message ***
 *
 * Revision 0.3  1999/05/25 20:58:00  spidey
 * Handles close() menu.
 * Changed name.
 *
 * Revision 0.2  1999/05/25 16:17:16  spidey
 * First working version. Menu with Quit working.
 *
 *********************************************************************/

#include "MWindow.h"
#include "Matrix.h"
#include "ToolBar.h"

#include <qkeycode.h>
#include <qpopupmenu.h>
#include <qmenubar.h>
#include <qapplication.h>
#include <qaccel.h>
#include <iostream.h>
#include <qpixmap.h>

MWindow::MWindow()
  : QMainWindow( 0, "Main Window" ) {  initialize(); }

MWindow::MWindow(QDataStream &stream) {  
  initialize();
  stream >> *matrix;
}

MWindow::~MWindow() {}

void MWindow::initialize() {

  setCaption( "Matrix Design : New Matrix" );
   
  fileMenu = new QPopupMenu(this);

  matrix = new Matrix (this);

  fileMenu->insertItem( "New", this, SLOT(newWindow()), CTRL+Key_N );
  fileMenu->insertItem( "Open", this, SLOT(load()), CTRL+Key_O );
  fileMenu->insertItem( "Save", this, SLOT(save()), CTRL+Key_S );
  fileMenu->insertItem( "Save as...", this, SLOT(saveAs()), CTRL+Key_V);
  fileMenu->insertItem( "Save as Image...", this, SLOT(saveImage()), CTRL+Key_I);
  fileMenu->insertItem( "Save as Text...", this, SLOT(saveReport()), CTRL+Key_R);
  fileMenu->insertSeparator();
  fileMenu->insertItem( "Randomize...", matrix, SLOT(randomize()), CTRL+Key_Z);
  fileMenu->insertItem( "Print", this, SLOT(print()), CTRL+Key_P );
  fileMenu->insertSeparator();
  fileMenu->insertItem( "Close", this, SLOT(closeWindow()), CTRL+Key_W );
  fileMenu->insertItem( "Quit", qApp, SLOT(quit()), CTRL+Key_Q );
  menuBar()->insertItem( "&File", fileMenu );

  optionMenu = new QPopupMenu(this);
  
  colorMenu = new QPopupMenu(optionMenu);
  colorMenu->insertItem( "Blue" );
  colorMenu->insertItem( "Green" );
  colorMenu->insertItem( "Orange" );
  colorMenu->insertItem( "Red" );

  optionMenu->insertItem( "Global Matrix Color", colorMenu);

  menuBar()->insertItem( "&Options", optionMenu );
  
  connect(colorMenu, SIGNAL(activated(int)), matrix, SLOT(setGlobalColor(int)));

  setCentralWidget(matrix);

  connect(matrix, SIGNAL(modified()), this, SLOT(modified()));
  
  connect(toolbar, SIGNAL(curTypeChg(int)), matrix, SLOT(setType(int)));
  connect(toolbar, SIGNAL(curColorChg(int)), matrix, SLOT(setColor(int)));
  connect(toolbar, SIGNAL(curRatingChg(int)), matrix, SLOT(setRating(int)));
  
}

void MWindow::modified() {

  fileMenu->setItemEnabled(4, TRUE);
  
}

void MWindow::newWindow() {
  MWindow *newWin = new MWindow();
  newWin->show();
}
  
void MWindow::closeWindow() {
  close(TRUE);
}

void MWindow::real_save() {

  QFile file (fileName);
  file.open(IO_WriteOnly);
  QDataStream stream(&file);
  stream << *matrix;
  matrix->unModify();
  fileMenu->setItemEnabled(4, FALSE);
    
}

void MWindow::saveReport() {

  QFile file( QFileDialog::getSaveFileName( 0 ) );
  file.open(IO_WriteOnly);
  QTextStream stream(&file);
  stream << "Matrix \"" << fileName << "\" Analysis" << endl
	 << "========================================"
	 << "========================================"
	 << *matrix;
  
}

void MWindow::saveImage() {

  QFile file( QFileDialog::getSaveFileName( 0 ) );
  file.open(IO_WriteOnly);
  QDataStream stream(&file);
  stream << matrix->getPixmap();

}

void MWindow::load() {
  QString f = QFileDialog::getOpenFileName( 0 );
  if ( !f.isEmpty() ) {
    QFile file (f);
    file.open(IO_ReadOnly);
    QDataStream stream(&file);
   //  disconnect(toolbar, SIGNAL(curTypeChg(int)), matrix, SLOT(setType(int)));
//     disconnect(toolbar, SIGNAL(curColorChg(int)), matrix, SLOT(setColor(int)));
//     disconnect(toolbar, SIGNAL(curRatingChg(int)), matrix, SLOT(setRating(int)));
//     matrix->close();
//     delete matrix;
//     cerr << "matrix deleted";
//     cerr << "newMat*=" << newMat << endl;
    
    MWindow *newWin = new MWindow(stream);
    //     Matrix *newMat = new Matrix(newWin);
    newWin->show();
    
    //    stream >> *matrix;
//     newWin.setCentralWidget(matrix);
//     matrix->show();
//     matrix->dumpObjectInfo();
//     matrix->dumpObjectTree();
    
//     connect(toolbar, SIGNAL(curTypeChg(int)), matrix, SLOT(setType(int)));
//     connect(toolbar, SIGNAL(curColorChg(int)), matrix, SLOT(setColor(int)));
//     connect(toolbar, SIGNAL(curRatingChg(int)), matrix, SLOT(setRating(int)));
    
  }  
}

void MWindow::print() {
}
