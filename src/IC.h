/****************************-*-c++-*-*********************************
 * $Id: IC.h,v 1.6 1999/09/17 02:58:22 spidey Exp spidey $
 **********************************************************************
 * The IC definition
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: IC.h,v $
 * Revision 1.6  1999/09/17 02:58:22  spidey
 * Added the IC() constructor
 *
 * Revision 1.5  1999/07/22 13:24:17  spidey
 * Serialization on QTextStreams
 *
 * Revision 1.4  1999/06/08 15:00:11  spidey
 * Added static get(Type|Color)Text()
 *
 * Revision 1.3  1999/06/01 19:48:44  spidey
 * Serialisation
 *
 * Revision 1.2  1999/06/01 16:54:23  spidey
 * Commented
 *
 * Revision 1.1  1999/05/28 17:58:14  spidey
 * Initial revision
 *
 *********************************************************************/

#ifndef IC_H
#define IC_H

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif
#include <qobject.h>
#include <qtextstream.h>

/**
 * White IC can hide gray IC
 * Set the rating to TRAPPED when so.
 */
#define TRAPPED -1

/**
 * These are the defs for the IC types
 */
#define WHITE 0
#define ACCESS 0
#define BARRIER 1
#define SCRAMBLE 2
#define GRAY 3
#define BLASTER 3
#define KILLER 4
#define TAR_BABY 5
#define TAR_PIT 6
#define TRACE 7
#define TRACE_REPORT 8
#define TRACE_DUMP 9
#define TRACE_BURN 10
#define BLACK 11

class IC : public QObject {

  Q_OBJECT
  
private:
  /**
   * An IC is a type and a rating
   */
  int type, rating;
  
public:

  /**
   * Empty basic IC
   */
  IC();
  /**
   * This creates a random IC in a node of the given color
   */
  IC(int);

  /**
   * This is a copy constructor
   */
  IC(IC* ic);

  /**
   * this creates a specific node
   */
  IC(int t, int r);

  /**
   * Returns the appropriate field
   */
  int getType() const;
  int getRating() const;

  /**
   * This contains the int->text color and type binding
   */
  const QString getColorText();
  static const QString getColorText(int);
  const QString getTypeText() const;
  static const QString getTypeText(int);

public slots:

  /**
   * Set the appropriate field
   */
  void setType(int t);
  void setRating(int r);

  /**
   * Set the appropriate field, randomly
   */
  void setRType(int color);
  void setRating();

signals:
  /**
   * These are sent when the appropriate field is modified
   */
  void chgType(int);
  void chgRating(int);
  
};

/**
 * IC stream method
 */
Q_EXPORT QTextStream &operator<< ( QTextStream &, const IC &);
Q_EXPORT QDataStream &operator<< ( QDataStream &, const IC &);
Q_EXPORT QDataStream &operator>> ( QDataStream &, IC &);

#endif
