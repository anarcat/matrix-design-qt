/****************************-*-c++-*-*********************************
 * $Id: ToolBar.cpp,v 0.3 1999/06/07 15:55:52 spidey Exp spidey $
 **********************************************************************
 * ToolBar
 **********************************************************************
 *   Copyright (C) 1999 Antoine Beaupr� <censored@domain.old>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  See also http://www.fsf.org
 **********************************************************************
 * $Log: ToolBar.cpp,v $
 * Revision 0.3  1999/06/07 15:55:52  spidey
 * Modified the layout a little
 *
 * Revision 0.2  1999/05/26 16:28:59  spidey
 * Added the layout and implemented slots/signals
 *
 *********************************************************************/

#include "ToolBar.h"
#include <qbuttongroup.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qcombobox.h>
#include <qlayout.h> 

ToolBar *toolbar = 0;

ToolBar::ToolBar()
  : QWidget (0, "toolbar") {  

  /**
   * Setup the rating field
   */
  QSpinBox *rating = new QSpinBox(this, "rating field");
  rating->setValue(3);
  rating->setFixedSize(QSize(35,20));
  QLabel *ratingl = new QLabel(rating, "&Rating:", this);  

  /**
   * Setup the color 'thing'
   */
  QComboBox *color = new QComboBox(FALSE, this);
  color->insertItem("Blue");
  color->insertItem("Green");
  color->insertItem("Orange");
  color->insertItem("Red");
  color->setCurrentItem(GREEN);
  color->setFixedSize(QSize(80,20));
  QLabel *colorl = new QLabel(color, "&Color:", this);
  colorl->setFixedSize(QSize(80,20));
  
  /**
   * Initialize all buttons
   */
  // the size of the buttons
  QSize buttonSize(40,20);

  QPushButton *cpu = new QPushButton("CPU", this, "cpu");
  cpu->setToggleButton(TRUE);
  cpu->setFixedSize(buttonSize);
  
  QPushButton *spu = new QPushButton("SPU", this, "spu");
  spu->setToggleButton(TRUE);
  spu->setFixedSize(buttonSize);

  QPushButton *san = new QPushButton("SAN", this, "san");
  san->setToggleButton(TRUE);
  san->setFixedSize(buttonSize);

  QPushButton *ds = new QPushButton("DS", this, "ds");
  ds->setToggleButton(TRUE);
  ds->setFixedSize(buttonSize);
  
  QPushButton *sn = new QPushButton("SN", this, "sn");
  sn->setToggleButton(TRUE);
  sn->setFixedSize(buttonSize);
  
  QPushButton *iop = new QPushButton("IOP", this, "iop");
  iop->setToggleButton(TRUE);
  iop->setFixedSize(buttonSize);
  
  /*
   * Layout Setup
   */
  QGridLayout *typeLay = new QGridLayout(this, 6,2);
  typeLay->addWidget(ratingl,0,0);
  typeLay->addWidget(rating,0,1);
  typeLay->addWidget(colorl,1,0);
  typeLay->addMultiCellWidget(color,2,2,0,1);
  typeLay->addWidget(cpu,3,0);
  typeLay->addWidget(spu,3,1);
  typeLay->addWidget(san,4,0);
  typeLay->addWidget(ds,4,1);
  typeLay->addWidget(sn,5,0);
  typeLay->addWidget(iop,5,1);
  
  /*
   * Button group setup
   */
  QButtonGroup *grp = new QButtonGroup();
  grp->setExclusive(TRUE);
  // Add the buttons to the group with their respective IDs from Node.h
  grp->insert(cpu,CPU);
  grp->insert(spu,SPU);
  grp->insert(san,SAN);
  grp->insert(ds,DS);
  grp->insert(sn,SN);
  grp->insert(iop,IOP);
  
  spu->setOn(true);

  adjustSize();

  connect(grp, SIGNAL(clicked(int)), this, SIGNAL(curTypeChg(int)));
  connect(rating, SIGNAL(valueChanged(int)), this, SIGNAL(curRatingChg(int)));
  connect(color, SIGNAL(activated(int)), this, SIGNAL(curColorChg(int)));
  show();
}

ToolBar::~ToolBar() {}
